# General:
WGSBAC is a pipeline for genotyping and characterisation of bacterial isolates utilizing whole-genome-sequencing (WGS) data. WGSBAC uses Snakmake for workflow-management and conda for easy installation of software.
 By combination of modules, species-specific or task-specific pipelines can be created. It uses paried-end NGS data (tested for MiSeq) or already assembled genomes as input.
 If you use this pipeline in a paper, don't forget to give credits to the authors by citing the URL of this repository.

# Modules:
 WGSBAC strongly depends on wonderful work of great people in the files of bacterial genomics. Here is a list of available modules. For details, please refere to the respective software. Also give credits to the software used within the modules by citing the respective paper or GIT.

Currently modules may perform the following:
1. QC using FASTQC https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
2. Calculating coverage adapted from https://github.com/raymondkiu/fastq-info/blob/master/fastq_info_3.sh
3. Assembly using Shovill https://github.com/tseemann/shovill
4. QC of assemblies using Quast https://github.com/ablab/quast
5. Annotation using bakta https://github.com/oschwengers/bakta
6. Checking for contamination between species by using kraken2 https://ccb.jhu.edu/software/kraken2/ and within species using ConFindR https://olc-bioinformatics.github.io/ConFindr/
7. Identification of genus, species and strain using sourmash https://sourmash.readthedocs.io/en/latest/ with GTDB https://gtdb.ecogenomic.org/
8. Genotyping with parSNP https://harvest.readthedocs.io/en/latest/content/parsnp.html
9. Genotyping with canSNPer https://github.com/adrlar/CanSNPer and CanSNPer2 https://github.com/FOI-Bioinformatics/CanSNPer2
10. Genotyping with snippy https://github.com/tseemann/snippy Build tress with RAxML  https://cme.h-its.org/exelixis/web/software/raxml/ or FastTree  http://www.microbesonline.org/fasttree/
11. Genotyping with MLST https://github.com/tseemann/mlst
12. Salmonella Serotyping with  SISTR https://github.com/phac-nml/sistr_cmd
13. Salmonella Serotyping with  SeqSero https://github.com/denglab/SeqSero
14. E.coli SerotypeFinder and EcOH (SRST2)
15. MLVA using  MLVA_finder https://github.com/i2bc/MLVA_finder
16. Pangenome using panaroo https://gtonkinhill.github.io/panaroo/
17. Prepare visualisation of trees using Itol https://github.com/mgoeker/table2itol
18. Hierarchical clustering of MLVA results and SNP distances from Snippy https://tinyurl.com/ybjklpvy
19. Hierarchical BAPS based on core-genome-SNP alignment https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6178908/
20. Predicton of AMR determinants using AMRFinderPlus https://github.com/ncbi/amr/wiki/Running-AMRFinderPlus and abricate https://github.com/tseemann/abricate with:
 a) Resfinder   https://cge.cbs.dtu.dk/services/ResFinder/
 b) CARD  https://card.mcmaster.ca/
 c) NCBI  https://www.ncbi.nlm.nih.gov/bioproject/313047
21. Predicton of virulence determinants using abricate https://github.com/tseemann/abricate with:
 a) VFDB (core) http://www.mgc.ac.cn/VFs/main.htm
 b) Salmonella Pathogenicity Islands https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database
 c) ecoli_vf database
22. Identification and characterisation of plasmids using abricate with Plasmidfinder https://cge.cbs.dtu.dk/services/PlasmidFinder/ and Platon https://github.com/oschwengers/platon
22. Clostridia toxin typing using  abricate with https://gitlab.com/FLI_Bioinfo_pub/perfringens-toxinotypes
23. Prediction of Arcobacter specific AMR and virulence determinants using https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri


<img src="dag.png" width="800" />

# Authors

* Jörg Linde (@joerg.linde)
* Mostafa Abdel-Glil (@Mostafa.Abdel-Glil)





# Install WGSBAC

To install WGSBAC, you need to install Miniconda, Snakemake and then download the source code.
All ohter dependencies are downloaded during run-time.

## Miniconda

    # Download conda installer
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
 
    # Set permissions to execute
    chmod +x Miniconda3-latest-Linux-x86_64.sh

    # Execute. Make sure to "yes" to add the conda to your PATH
    ./Miniconda3-latest-Linux-x86_64.sh 		

    # Add channels
    conda config --add channels defaults
    conda config --add channels conda-forge
    conda config --add channels bioconda

## Snakemake (Version 5.2.2 or newer)

a) By installing system-wide

    conda install -c bioconda -c conda-forge snakemake>=5.2.2

OR

b) By installing in an independent environment

    conda create -n snakemake -c bioconda -c conda-forge snakemake>=5.2.2
    conda activate snakemake

If you run into problems during installation, you can also try with the help of [mamba](https://github.com/mamba-org/mamba)

    conda install -n base -c conda-forge mamba
    conda activate base
    mamba create -c conda-forge -c bioconda -n snakemake snakemake>=5.2.2

## WGSBAC
Install GIT:  

    https://gist.github.com/derhuerst/1b15ff4652a867391f03


Download WGSBAC:

    git clone https://gitlab.com/FLI_Bioinfo/WGSBAC.git

Note: all users who want to use WGSBAC need to have read and write access to the instalation folder.

Add WGSBAC to PATH in conda environment:


### Minkraken2 or Kraken2
  If not already available, download [Minikraken2](ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/) to a path available from the snakemake conda environment (kraken2DB folder).

### Optional Databases

ConFindr is a tool that can detect contamination in bacterial NGS data, both between and within species. If you plan to use, download and provide ConFindR rMLSt database as descirbed https://olc-bioinformatics.github.io/ConFindr/install/

Bakta is a tool for the rapid & standardized annotation of bacterial genomes and plasmids. If you plan to use, download the database as described here https://github.com/oschwengers/bakta


# Running WGSBAC

Running only with obligatory parameters, WGSBAC will run FASTQ, calculate coverage, assembles genomes,run Quast on genomes and identify the genus and species using Kraken and MASH.

`wgsbac.pl --table <meta_data_table>  --results <absolute_path_to_resultsfolder> --kraken <path_to_kraken2_folder>`

Everyting between "<>" needs to be replaced with correct pathes or file names.



###  Example Data and  Usage

Find a detailled example on usage here: [Example](example/README.md)

short examples

Brucella


`wgsbac.pl --table <meta_data_table>  --results <path_to_resultsfolder> --kraken <path_to_kraken2_folder> --mlva Brucella --snippy --conf <path_to_rMLST> --run`


Bacillus

`wgsbac.pl --table <meta_data_table>  --results <path_to_resultsfolder> --kraken <path_to_kraken2_folder> --cansnper Bacillus_anthracis  --mlva Anthracis --snippy  --virulence --conf <path_to_rMLST>  --cansnper2 bacillus_anthracis.db --run`

Campylobacter

`wgsbac.pl --table <meta_data_table>  --results <path_to_resultsfolder> --kraken <path_to_kraken2_folder>  --mlst campylobacter  --conf <path_to_rMLST>   --snippy --plasmids --virulence --amr --amrorganism Campylobacter --run`

Francisella

`wgsbac.pl --table <meta_data_table>  --results <path_to_resultsfolder> --kraken <path_to_kraken2_folder> --snippy   --cansnper Francisella --cansnper2  francisella_tularensis.db --conf <path_to_rMLST>  --run`


Salmonella

`wgsbac.pl --table <meta_data_table>  --results <path_to_resultsfolder> --kraken <path_to_kraken2_folder> --mlst senterica_achtman_2 --conf <path_to_rMLST>  --sistr --seqsero  --snippy --spi  --virulence --plasmids --amr --amrorganism Salmonella --run`

E.coli

`wgsbac.pl --table <meta_data_table>   --results <path_to_resultsfolder> --kraken <path_to_kraken2_folder>  --mlst ecoli   --ecolitype --amr --amrorganism Escherichia  --virulence --plasmids --snippy  --virulence`


## Parameters:


### Obligatory Parameters:

--(t)able String.Complete path to metadata mable (tab delimited) See chapter Metadata

--(k)raken String. Complete path to kraken2DB folder.

### Optional Parameter

--(h)elp Helpful message

#### Optional Configuration of Pipeline


--(r)esults  String. Absolute path to result directory. Standard <current_dir>/result/

--(l)og  String. Complete path to log directory. Standard results/log

--run Flag. If used pipeline is started automatically.

--cpus String. Number of cpus to run the pipeline. Standard is 40.  Only needed when using option run


#### Optional Configuration of Assembly#


--(ad)atrim String. If used adapters are trimmed by trimmomatic

--min(cov) Integer. Filtering of assembly with minimum contig k-mer coverage minimum contig length.

--(min)len Integer. Filtering of assembly minimum contig length.



####  Optional Serotyping

--(si)str Flag. If used Sistr is run for Salmonella serotyping.

--(se)qsero Flag. If used SeqSero is run for Salmonella serotyping.

--(ecoli)type Flag. If used identifies Clermont phylogrups and runs abricate against databases ecoh  and ecoli_vf.

####  Optional Genotyping

--conf String. Absolute path to ConFindR rMLST database. If used ConFindR is run. \n

--(ml)st  String. If used mlst is run. Select a scheme from https://github.com/tseemann/mlst/blob/master/db/scheme_species_map.tab.

--(c)ansnper String. If used cansnper is run to predict canonical SNP types. Select a species from data/cansnper_species.txt.

--(c2)ansnper2 String. Name of CanSNPer2 database. Currently supports: bacillus_anthracis.db, francisella_tularensis.db, yersinia_pestis.db. If used CanSNPer2 is run to predict canonical SNP types

--(pa)rsnp Flag.If used parsnp is run.

--(sn)ippy Flag. If used snippy is run.

--(r)axml Flag. If used RAxML is run based on snippy core and/or panaroo.

--(pa)anaroo Flag. If used panaroo is run.This options needs bakta and the bakta DB

--(ml)va String. One of 'Anthracis', 'Brucella', 'Burkholderia', 'Coxiella'. If used runs MLVA_finder https://github.com/i2bc/MLVA_finder\n

####  Optional Phenotyping
--(amr) Flag if used  runs AMRfinderplus and abricate against CARD, NCBI and resfinder.

--amr(org)anism String Option for AMRfinderplus. You may use organism-sepcific results for: Campylobacter,Enterococcus_faecalis, Enterococcus_faecium, Escherichia,Klebsiella,Salmonella,Staphylococcus,Vibrio_cholerae. Find etailts https://github.com/ncbi/amr/wiki/Running-AMRFinderPlus#--organism-option

--(vir)ulence FLag if used runs abricate against VFDB core.

--(plas)mids FLag if used runs a) abricate against plasmidfinder DB b) platon

--pathislands(spi) Flag  If used will run  abricate using database for Salmonella pathogenicity islands database https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database

--clostox(ct)  Flag  If used will run abricate using  database for Clostrodia toxing typing https://gitlab.com/FLI_Bioinfo_pub

--arcodb(adb) Flag  If used will run abricate using Aliarcobacter butzleri - AMR and virulence databases https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri

--(ba)kta String.  Complete Path to Bakta DB. If used assembled genomes are annotated using Bakta. https://github.com/oschwengers/bakta

--(pr)okka Flag. If used assembled genomes are annotated using Prokka.


#### Species-specific modules

Overview about recommended(**r**) and possible(**p**) modules for selected genera.

| Genus | amr | virulence | plasmids| bakta | mlst | parsnp |cansnper|cansnper2|snippy| mlva| panaroo| sistr|seqsero|spi|clostox|arcodb
|---------------|----------|-------|-----------|--------|------|--------|--------|------|-----|------|------|-------|-------|-------|-------|-------|
|Acinetobacter |r|r|r|p|r|p|||r|||||||
|Arcobacter |p|p|r|p|p|p||r|||r|||||r
|Bacillus |p|r|p|p|p|p|p|p|p|p||||||
|Brucella |p|p|p|p|p|p|p|r||p||||||
|Campylobacter |r|r|r|p|p|p|||r||r|||||
|Clostridia |p|p|p|p|p|p||r|||p||||r|
|Coxiella |p|p|p|p||p||r|p|||||||
|Klebsiella |r|r|r|p|r|p|||r||p|||||
|Francisella |p|p|p|p|r|p|r|r|r|||||||
|Salmonella |r|r|r|p|r|p|||r|p|p|r|r|r||
|Yersinia |r|r|r|p|p|p||r|p|p|p|||||









# Metadata

Besides Rawdata (fastQ and assemblies), the workflow needs a metadata-template as input which gives information about names of isolates, their location and type.
See [example/example_meta.csv](example/example_meta.csv)  for an example.

Metadata is a tab-separated file with the following columns in given order and with given column headings:

1. ID=ID of Isolate. No whitepaces, dots (.) or underscores (_) allowed.
2. Common.name=Common name of isolate (as used in figures). No whitepaces, dots (.) or underscores (_) allowed.
3. Type: any entry of: reference, new or type (exactly one)
4. File_left_reads:  path to file of left read pair (R1, _1)
5. File_right_reads: path to file of right read pair (R2, _1)
6. File_assembly:  path to assembled genome
7. Lat.Long.of.Isolation: optinal. Latitude and Longitude of Isolate separated by ",". If missing isolate will not appear in map.

Considering 3.type:
This columns is used to visualise isolates in trees and figures. You may differentiate strains being part of surveillance (f.e. 'collected', of an actual outbreak (f.e. 'outbreak').  

For each sample, you may either give FASTQ files as input (column 4 and 5) or assembled genomes (column 6). Both input types for teh same sample is not allowed.


Exactly one isolate needs to be marked as reference. This should be a high-quality reference genome (fasta file), but must not be the type strain. It is used as reference to control quality of assemblies and as reference to creat trees. A genome sequence (File_assembly) is needed for the type stain.

After column 7, you may add additional metadata as needed (Country, City, Host,...). Please do not forget column headings.

The script folder2metada.pl may help to create  metadata tables from a folder with fastQ files and assembly files. Below you find help and an example.

    perl <WGSBACfolder>/scripts/folder2metada.pl --help
    perl <WGSBACfolder>/scripts/folder2metada.pl --table meta.csv --folder /home/WGSBac




####   Adding new samples or analyses(modules)

In case you have run the pipeline for your samples and now want to add further samples to your analysis, simply add them to the metadata table and run WGSBAC again using the same results folder. Thanks to Snakemake, WGSBAC will not analysis all samples again but only the new samples and will updated all figures and tables.


In case you have run the pipeline but now want to add further analysis by tools/modules not used so far, simply add those parameters to wgsbac.pl and use the same results folder. Thanks to Snakemake, WGSBAC will then use the allready existing data as input for the new tools instead of starting from the beginning.





## Results

Here is an overview of the folder structure for most important results.  The file multiqc/multiqc_report.html  is a summary of all results available as tables. The folder trees holds all created phylogenetic trees in Newick (nw) format.
For tree visualization, we recommend itol (https://itol.embl.de/). The folder trees/itol/ holds configuration files to create color bares in itol for each columns in the metadata table. Where possible, simple tables (CSV, XLS) summarizing the results of each tool are available.


```
results/
|-- map.pdf
|-- trees
|   |-- snippy.nw
|   |-- itolclust
|   |-- itolmeta
|   |--clustering
|-- multiqc
|   |-- multiqc_report.html
|-- multiqc_fastqc/
|   |-- fastqc_assembly_report.html
|-- cansnper
|   |-- allcanSNPer.csv
|   |-- allcanSNPer.xlsx
-- cansnper2
|   |-- allcanSNPer2.csv
|   |-- allcanSNPer2.xlsx
|-- quast
|   |-- allquast.csv
|   `-- allquast.xlsx
|-- coverage
|   |-- allcoverage.csv
|   |-- allcoverage.xlsx
|-- amr
|-- virulence
|-- plasmids
|-- fastq
|-- fastqc
|-- panaroo
|-- MLVA_finder
|-- sourmash
|-- coverage
|-- Seqsero
|-- finalAssembly
|-- kraken2
|-- sistr
|-- parsnp
|-- bakta
|-- shovill
```

## Logging

WGSBAC performs extensive logging including logs for each module and tool. As standard, logs are saved in /results/log but you may change this folder. In addition, Snakemake's report function is used to create  SnakemakeReport.html,  HTML based report including exact version of used tools (click on +).


## Expert options

1. Adapt output folder structure: You may change the result folder structure by editing:  [defineFolders.Snakefile](snakefiles/defineFolders.Snakefile)

2. Detailed parameter of tools:  wgsbac.pl creates a config.yaml file which is used to configure Snakemake. Config.yaml holds pathes to databases and standard parameters for the used tools (f.e. maximal number of SNPS for a cluster).
Moreover, config.yaml contains an additional empty parameter (f.e.  parsnp_params) for each tool. This parameter can be used to add setting and parameters. Note that this setting is directly parsed to the tool as string.
