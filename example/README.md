# Example workflow

This is a toy example to illustrate features of the pipeline. It focuses on Francisella tularensis but principles are the same with other speices. See  wgsbac.pl --help for example calls for other species.
The used data and meta-data are NOT connected and results do NOT make biomedical sense.


The example assumes a correct installation in the folder "\<WGSBACfolder\>". You may run it from every folder you want but you need write access to WGSBAC installation folder

## Install sra-toolkits

For the example, you need to install and configure sra-toolkits  as described https://github.com/ncbi/sra-tools/wiki/01.-Downloading-SRA-Toolkit


## Download needed data

Create a folder for testing and change into folder

    mkdir testWGSBAC
    cd testWGSBAC

Download Minikraken

    wget ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken_8GB_202003.tgz

  Unzip (extract) Minikraken

    tar xfvz minikraken_8GB_202003.tgz


Download raw data

    <path to sra toolkit>/fastq-dump --split-3 SRR653896 SRR653895 SRR653894 SRR653893


Download high quality genome assembly which we will  use as type strain

    wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/008/985/GCF_000008985.1_ASM898v1/GCF_000008985.1_ASM898v1_genomic.fna.gz

Unzip (extract) assembly

    gunzip GCF_000008985.1_ASM898v1_genomic.fna.gz

Create a metadata table

    perl <path to WGSBAC folder>/scripts/folder2metada.pl --table meta.csv --folder .


Edit the table by hand. Change the third column (Type):
1. Change Type into 'reference' for the downloaded high quality genome.


See [example/example_meta.csv](example/example_meta.csv)  for anticipated template.




## Run WGSBAC

### Optional: activate snakemake environment

Only(!), in case  snakemake is installed in an independent environment
    
    conda activate snakemake

### Start WGSBAC
    
    wgsbac.pl --table meta.csv --results results/ --kraken minikraken_8GB_20200312/ --cansnper Francisella --run



This command will directly run the pipeline (--run option) without changing expert parameters. It will use 40 cores as standard. You may change this by using -- cpus.

As standard WGSBAC will perform: quality controll (FASTQC), coverage calculation, assembly with Shovill, quality of assembly with Quast, identification of genus and species with Kraken2 und sourmash.

Due to addtional optional parameters, WGSBAC will run CanSNPer (--cansnper) using the Francisella database.

### Add tools

Assume after anaylising results, you additionally want to check for CanSnper2. To add new results using new modules(tools), just run the pipeline with the same results folder. With help of Snakemake, WGSBAC will not start from beginning but only calculate what is needed for your new results.

     wgsbac.pl --table meta.csv --results results/ --kraken minikraken_8GB_20200312/ --cansnper Francisella --cansnper2  francisella_tularensis.db --run


  Anticipated results :

  The pipeline produces a number of results. Main results can be found in the folder [example/results](example/results) SnakemakeReport.html is HTML based report including exact version of used tools (click on +).

  The file multiqc/multiqc_report.html  is a summary of all reasults available as tables. Where possible, simple tables (CSV, XLS) summarizing the results of each tool are available. The folder trees holds all created phylogenetic trees in Newick (nw) format.



```
results/
|-- virulence
|   |-- all_vfdb.xls
|-- cansnper
|   |-- allcanSNPer.csv
|   |-- allcanSNPer.xlsx
|-- coverage
|   |-- allcoverage.csv
|   |-- allcoverage.xlsx
|-- kraken
|-- multiqc
|   -- multiqc_report.html
|-- multiqc_fastqc
|   `-- fastqc_assembly_report.html
|-- quast
|   |-- allquast.csv
|   |-- allquast.xlsx
|-- trees
|   |-- snippy.nw
|   |-- itolclust
|   |-- clustering
|-- snippy
|   |-- core
|   |   |-- core.aln
|   |   |-- core.full.aln
|   |   |-- core.ref.fa
|   |   |-- core.tab
|   |   |-- core.txt
|   |   |-- core.vcf
|   |   |-- Snipdist_mqc.txt
|   |   |-- snpdist.csv
|   |   `-- snpdist.xls
|   |-- SRR653893
|   |-- SRR653894
|   |-- SRR653895
|   `-- SRR653896
|-- sourmash
|   |-- allSourmash.csv
|   |-- allSourmash.xlsx
  ```
