# $1 - WGSBAC resulsfold 
#usage sh /home/software/Skripte/clean_WGSBAC_results.sh {WGSBACresultsfolder}


rm -rf   $1/quast/*/icarus_viewers
rm -f   $1/kraken/*/*/all.kraken

rm -rf   $1/snippy/*/snps.aligned*
rm -rf   $1/snippy/*/snps.consensus* 
rm -rf   $1/snippy/*/reference/
rm -rf   $1/snippy/*/*.bam


rm -f   $1/rawassembly_res/*/contigs.gfa
rm -f   $1/rawassembly_res/*/spades.fasta
rm -rf   $1/tmp/
rm -rf   $1/fastq/





