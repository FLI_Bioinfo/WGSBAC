require(writexl)


#loggin
log=file(snakemake@log[[1]],open="wt")
sink(log)
sink(log, type = "message")#logs all output of R into snakemake log

abricateres=read.delim(snakemake@input[["abricateres"]],as.is=T)#read abricate suammry file

#abricateres=read.table("tmp/abricate/plasmidfinder.csv",sep="\t",as.is=T,header=T,comment.char = "~")
tmp=strsplit(abricateres[,1],"/")
for (i in 1:length(tmp)) abricateres[i,1]=tmp[[i]][[length(tmp[[i]])]]

db=snakemake@params[["db"]]
abricateres[,1]=gsub(paste("_",db,".csv",sep=""),"",abricateres[,1])#removes results/abricate/ from ID
#tmp=strsplit(abricateres[,1],"_") #split at "/" to remove "vfdb", plasmidfinder or somilar from string
#ID=rep(NA,length(tmp))#always take first element after splitting which is the ID
#for ( i in 1:length(ID)) ID[i]=paste(tmp[[i]][[1]]
cat("read file abricateres\n")

  metaData <- read.delim(snakemake@input[["metadata"]],dec=".",sep="\t",as.is=T)#read metadata
metaData<-metaData[,c(1,2)]#take only ID and name
cat("read meta data\n")
abricateres=merge(abricateres,metaData,by.x=1,by.y=1,all.y=F,all.x=F)#merge with metadata to add common name

abricateres=abricateres[,c(1,ncol(abricateres),2:(ncol(abricateres)-1))]#reorder columns

colnames(abricateres)[1]="ID"
if(ncol(abricateres)==3){
  colnames(abricateres)=c("ID","Common.name","NUMB")
}else{
  colnames(abricateres)[4:ncol(abricateres)]=paste("Coverage",colnames(abricateres)[4:ncol(abricateres)],sep=" ")
}

write.table(abricateres,snakemake@output[["abricate_csv"]],row.names=F,sep="\t",quote=F)
write_xlsx(abricateres,snakemake@output[["abricate_xls"]])
cat("successfully created XLS \n")


# write mqc report (for multiQC)

title <- "# title: 'Abricate'"
section <- "# section: ''"
desc <- "# description: ''"
format <- "# format: 'tsv'"
plot_type <- "# plot_type: 'table'"
#config<-"# pconfig:"
#id<-"#    id: 'Coverage from fastq_info'"
#ylab<-"#    ylab: 'Coverage'"


writeLines(c(title, desc, section, format, plot_type),snakemake@output[["abricate_mqc"]] )
write.table(abricateres, file=snakemake@output[["abricate_mqc"]], quote=FALSE, sep="\t", row.names=FALSE, col.names=TRUE, append=TRUE, qmethod = c("escape"))
cat("successfully created header for MultiQC \n")


##
