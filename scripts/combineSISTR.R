#!/usr/bin/env Rscript
# Load package "writexl" to export data-frames to Excel
require(writexl)

#loggin
log=file(snakemake@log[[1]],open="wt")
sink(log)
sink(log, type = "message")#logs all output of R into snakemake log

# Get a list of all sistr files in each directory of a sample (*PM) ending with the pattern (.tab)
#fileList <- list.files(path = "results/sistr/",pattern="sistr-output", recursive=TRUE)
fileList <- c(snakemake@input[["sistres"]])
cat("read file list\n")#list SISTR results

# Read metadata
metaData <- read.delim(snakemake@input[["metadata"]], dec=".", sep="\t", as.is=T)
cat("read meta data\n")


# Read all sistr files and create a list of tables
dataList <- lapply(fileList,function(x) tryCatch(read.delim(x,as.is=T,header=T),error=function(e) NA))#write NA for empty files
dataList <- dataList[!(is.na(dataList))]#removes empty files

cat("read data list\n")


# Combine each table in the list into a single table
allData <- do.call("rbind",dataList)
cat("merged data\n")
allData=allData[,c(7,14,9:11,15:17,1:6,12,13)]#reorder cols to have ID in first
allData[,1]=gsub(".fasta","",basename(allData[,1]))
allData=merge(metaData[,1:2],allData,by.x=1,by.y=1,all.y=T,all.x=T) #combines IDs with common name
# Write the results
#write.table(allData, file = "/home/silvia.garcia/snake_francisella/results/sistr/all.csv", row.names=F,sep="\",quote=F)
#write_xlsx(allData, path = "/home/silvia.garcia/snake_francisella/results/sistr/all.xls", col_names = T, format_headers = T)
write.table(allData,snakemake@output[["allcsv"]],row.names=F,sep="\t",quote=F)
write_xlsx(allData,snakemake@output[["allxlsx"]])
cat("sucessfully finnished \n")

title <- "# title: 'SISTR_results'"
section <- "# section: ''"
desc <- "# description: 'Rapidly Typing and Subtyping for Salmonella'"
format <- "# format: 'tsv'"
plot_type <- "# plot_type: 'table'"
#
allData=allData[,-c(2:5,7,11,12)]#remove columns which are too long for multiqc

writeLines(c(title, desc, section, format, plot_type),snakemake@output[["multiqc"]])
write.table(allData, file=snakemake@output[["multiqc"]], quote=FALSE, sep="\t", row.names=FALSE, col.names=TRUE, append=TRUE, qmethod = c("escape"))
cat("successfully created header for MultiQC \n")
