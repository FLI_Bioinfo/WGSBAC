#!/bin/bash
#dependencies
#mash
#seqkit
fastq1=$1  #fastq input 1
fastq2=$2  #fastq input 2
fasta=$3 #fasta input

readcount1=$(cat $fastq1| echo $((`wc -l`/4))) #read count in file 1
readcount2=$(cat $fastq2| echo $((`wc -l`/4))) #read count in file 2
total_seq=$(echo "$readcount1+$readcount2"|bc) #calculate total amount of  reads

readlength1=$(awk 'NR % 4 == 2 { s += length($1); t++} END {print s/t}' $fastq1) #calculate read length for fastq file 1
readlength2=$(awk 'NR % 4 == 2 { s += length($1); t++} END {print s/t}' $fastq2) #calculate read length for fastq file 2
totalreadlength=$(echo "$readlength1+$readlength2"|bc) #calculate total read length
averagereadlength=$(echo "$totalreadlength/2"|bc) #calculate average read length by parsing into bc command - which truncates the decimal numbers by default in multiplication/division
#To calculate genome size

if [ ${#fasta} -gt 0 ] 
then 
  genomesize=$(grep -v '^>' $fasta |wc -c)
else  
  mashfile=$(mktemp)
  mash sketch -o $(mktemp) -k 32 -m 3 -r $fastq1 &> $mashfile
  genomesize=$(egrep -Eo 'Estimated genome size:.{1,50}' $mashfile | cut -d " " -f 4- |  perl -ne 'printf "%d\n", $_;')
fi



coverage=$(echo "$total_seq*$averagereadlength/$genomesize"|bc)

filenameshort=$(basename -- "$fastq1")
filenameshort2=$(basename -- "$fastq2")

if [ ${#fasta} -gt 0 ]; then  filenameshort_ref=$(basename -- "$fasta")
else  
filenameshort_ref="NA"
fi






#To print the tab-delimited table on standard output
echo -n -e "File R1\t"
echo -n -e "File R2\t"
echo -n -e "Reference Genome\t"
if [ ${#fasta} -gt 0 ]; then  echo -n -e "Reference genome size\t"
else  
echo -n -e "Estimated genome size\t"
fi
echo -n -e "Average read length\t"
echo -n -e "Total reads\t"
echo -e "Theoretical coverage"
echo -n -e "$filenameshort\t"
echo -n -e "$filenameshort2\t"
echo -n -e "$filenameshort_ref\t"
echo -n -e "$genomesize\t"
echo -n -e "$averagereadlength\t"
echo -n -e "$total_seq\t"
echo -e "$coverage"


exit 0;
