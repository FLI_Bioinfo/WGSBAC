#!/usr/bin/perl
# Author: Jörg Linde
#
# creates metadata table from folder including genome assemblies and raw fastq datasets

use Getopt::Long qw(GetOptions);


my $usage="creates metadata table from folder including genome assemblies and raw fastq datasets\n
Usage: $0 --(f)older  --(t)able <metatdata.csv> \n Where:\n
--folder complete absolute path to folder having assemblies (fasta,fna,fa,fsa,fs,fnn) and paired-end FASTQ data. Might be gzipped (*_<R>1.fastq<.gz>, *_<R>2.fastq<.gz>)
--table Metadata Table (Tab delimited) holding columns:ID,Common.name,Type,File_left_reads,File_right_reads,File_assembly,Lat.Long.of.Isolation\n
        Type describes type of isolate and can be any of:a)  collected, b) outbreak c) reference [only once]\n
        File_left/right_reads hold path to gziped raw sequencing readsinf FASTQ format\n
        File_assembly for allready assembled reference strains (not needed if FASTQ files exist)\n

";
$help="";
    GetOptions(
        'folder|f=s' => \$folder,
        'table|t=s' => \$table,
        'help|h=s' => \$help,
    ) or die ("$usage");



if($help ne "") {
  die ("$usage");

}

if($folder eq ".") {#if "." was used as folder, get complet path
 $folder = qx(pwd);
 chomp $folder;
 $folder = $folder."/";
}

if(substr($folder,-1) eq "/"){#removes taling "/" in folder name
  chop($folder);

}

print "$folder\n";
opendir(DIR, $folder) or die $!;#opens fodler to read

$runningID=1;##will be used give systematic IDs for Isolates if can not guesse from file name
my %usedids;#checks if IDs where used already

#function guess ID from first part of file names
#if not possible add own ID
sub guessID{
 my $subfile=shift;#take argument (file name)
 my @tmp=split("_",$subfile);#splits at _
 my $ID="";
 if(($subfile =~ m/fastq/ | $subfile =~ m/fq/) && scalar(@tmp)>1){#fastqfile
   $ID=$tmp[0];#first element#take first element after split at "_"
 }
 else{#non fatsq file
    @tmp=split(/\./,$subfile);#splits at .
    $ID=$tmp[0];#take first element
#   $ID="Isolat_".$runningID;#generate systematic ID
#   $runningID++;
#   print "Can not automatically gues ID of $subfile. Using $ID\n"
 }
 if ($ID =~ m/-/) {
   @tmp=split("-",$ID);#splits at _
   $ID=$tmp[0];
  }
 if(exists $usedids{$ID}){ #check if ID has been used alread
    $ID="$ID.$runningID"; #add runnign number
    $runningID++;
  }
 $usedids{$ID} =1;##add key to used IDs
 $ID =~ s/\.//g; #removes . within ID  as it causes problems with some tools
 $ID =~ s/_//g; #removes _ within ID  as it causes problems with some tools
 $ID =~ s/\s//g; #removes whitespaces within ID  as it causes problems with some tools
return $ID
}

while (my $file = readdir(DIR)) {#reads files from folder per line
  if($file =~ m/fastq|fq/){#fastq File
#    if(!($file =~ /\.gz$/i)) {  #file is not gzipped
#      print "gzipping $file\n";
#      system("gzip -9 ".$folder."/".$file);
#      $file=$file.".gz";
#    }
    if($file =~ m/_1/ || $file =~ m/_R1/){#left read pair
      push @left, $file;#add to array of left files
    }elsif($file =~ m/_2/ || $file =~ m/_R2/){#right read pair
      push @right, $file;#add to array of rigth files
    }else{
      print "Unclear if FastQ file is left or right part of pairs. Please use _1/2 or _R1/2 in name. Skipping file $file\n"
    }
  }
  elsif($file =~ m/fasta|fna|fa|fsa|fs|fnn/){ #if file matches on of those ending its an assembly
      if($file =~ /\.gz$/i) { #file is gzipped
        print "gunzipping $file\n";
        system("gunzip ".$folder."/".$file);
        $file = $file =~ s/.gz//r;
      }
      push @assembly, $file;#add to array of assemblies

}else{
  print "File type unkwown. Skipping file $file\n"


  }
}

@left=sort @left;#sorts read pairs so that they are next to each other
@right=sort @right;#sorts read pairs so that they are next to each other


open(OUT,'>', $table) or die("Can't open current file: $!"); #opens file or gives errormessage if not possible
print OUT "ID\tCommon.name\tType\tFile_left_reads\tFile_right_reads\tFile_assembly\tLat.Long.of.Isolation\n";#print header

for my $i (0 .. $#left) {#loop over all fastq files
  $locid=guessID($left[$i]);
  print OUT "$locid\t$locid\toutbreak\t$folder"."/"."$left[$i]\t$folder"."/"."$right[$i]\t\t\n"#print in table

}
for my $i (0 .. $#assembly) {#loop over all assembly files
  $locid=guessID($assembly[$i]);
  print OUT "$locid\t$locid\toutbreak\t\t\t$folder"."/"."$assembly[$i]\t\t\n"#print in table

}
