package WGSBAC::Checkmeta;
use strict;
use warnings;
use Exporter qw(import);
our @EXPORT_OK = qw(checktable);


#do some quality checks of input
#check for unwanted characters in IDs or name
#check for missing IDs or names
sub checktable {
  my ($table,$snippy,$parsnp) = @_;
  my %usedids;#checks if IDs where used already
  open FILE, "$table" or die $!;#opens input file (metadata table)
  my  $nrline=0;
  my $ref=0;
  while (my $line=<FILE>){  #reads table per line
    $nrline++;
    next if $nrline==1;#skips header line
    my  @content=split(/\t/,$line);#seperates line by TAB
    if(exists $usedids{$content[0]}){
        die "No duplicated ID allowed  $content[0]\n";
    }

    $usedids{$content[0]} =1;##add key to used IDs

    if($content[0] =~ /\./){#checks for . with in ID
      die "No . allowed in ID  $content[0]\n";
    }
    if($content[0] =~ /\-/){#checks for . with in ID
      die "No - allowed in ID  $content[0]\n";
    }

    if(length($content[0]) == 0 || length($content[1])==0){#checks for . with in ID
      die "Please specify ID and name for isloate in line $nrline\n";
    }
    if($content[0] =~ /\s/){#checks for whitespace with in ID
      die "No whitespace allowed in ID  $content[0]\n";
    }


    if($content[1] =~ /\./){#checks for . with in name
      die "No . allowed in name  $content[1]\n";
    }
    if($content[1] =~ /\s/){#checks for whitespace with in name
      die "No whitespace allowed in name  $content[1]\n";
    }
    if($content[1] =~ /_/){#checks for whitespace with in name
      die "No _ allowed in name  $content[1]\n";
    }
    if($content[0] =~ /\s/) {#no ID detected
  		die "Line $nrline has no ID. Skipping line\n";
  	}
    if(!($content[3] eq "") && !($content[5] eq "")) {#no fastq and fasta
  		die "Please add either one assembly file or two fastq-files for isolate $content[0].\n";
  	}
    if ($content[5] eq "" && !($content[3] =~ /\.fastq.gz$/i || $content[3] =~ /\.fq.gz$/i || $content[3] =~ /\.fastq$/i || $content[3] =~ /\.fq$/i)){
      die "Unknwon file type for FASTQ-files of isolate $content[0]. Allowed file-types are fastq,fq,fastq.gz,fq.gz\n"
    }
    if ($content[3] eq "" && !($content[5] =~ /\.fna$/i || $content[5] =~ /\.fasta$/i)){
      die "Unknwon file type for fasta-file of isolate $content[0]. Allowed file-types are fna and fasta\n"
    }
    
    if($content[2] eq "reference" && $ref==1){#checks for more than one ref strain
      die " Please define only one strain as 'reference' in column 'type'\n "
      }
    
    if($content[2] eq "reference"){#checks for ref strain
      $ref=1;#ref strain found
      }
   }
  close FILE;
  if($ref ==0 && ($snippy ne "" |$parsnp ne "" )){
      die "Reference strain needed for snippy or parsnp.  Please define one strain as 'reference' in column 'type' in metadata\n "
  }
  return ($ref)
}
1;
