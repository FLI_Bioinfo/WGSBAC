package WGSBAC::Checkparams;
use strict;
use warnings;
use Exporter qw(import);

our @EXPORT_OK = qw(checkparams);

#helper checks if string is element of file
#used to see if user selected correct species for canSNP
#user to see if user selected existing mlst profile



#checks for correct parameter setting
sub checkparams {
  my ($table,$resultsfolder,$binfolder,$kraken,$mlst,$cansnper,$cansnper2,$mlst_schemes,$cansnp_species,$mlva,$amrfinder,$organism,$bakta,$panaroo) = @_;
  if($table eq "") {
      die "mandatory argument --table missing "

  }

  unless (-e $table) {
      die "File $table doesn't exist";
    }

  if($resultsfolder eq "") {
    die "mandatory argument --results missing "

  }
  if($amrfinder) {
    if(length($organism)>0){
     if (!($organism=~ /Campylobacter|Enterococcus_faecalis|Enterococcus_faecium|Escherichia|Klebsiella|Salmonella|Staphylococcus|Vibrio_cholerae/)) {

    die "argument --amr(org)anism must be one of:Campylobacter,Enterococcus_faecalis, Enterococcus_faecium, Escherichia,Klebsiella,Salmonella,Staphylococcus,Vibrio_cholerae\n";
  }}
  }

  if($binfolder eq "") {
    die "Cannot find WGSBAC binaries."

  }
  if($kraken eq "") {
    die "mandatory argument --kraken missing. Add complete path to kraken2DB folder. You may download Minikraken2 here ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken2_v2_8GB_201904_UPDATE.tgz ";
    #check if mandatory fiels for kraken exist in folder
  }
  if(!(-d $kraken )){
    die "Folder $kraken does not exist\n"
  }
  my $hash=0;
  my $taxo=0;
  my $opts=0;
  opendir(DIR, $kraken ) or die $!;#loop torough all files in folder
  while (my $file = readdir(DIR)) {
      if ($file =~ m/hash.k2d/) {
        $hash=1;}#hash.k2d found
      if ($file =~ m/opts.k2d/){
       $opts=1;}#opts.k2d found
      if ($file =~ m/taxo.k2d/){
         $taxo=1;}#taxo.k2d found
  }
  if (!($hash&&$taxo&&$opts)) {
      die "input files hash.k2d, opts.k2d and taxo.k2d for Kraken missing in $kraken"
    }

  if($mlva ne "" && ($mlva ne "Burkholderia" && $mlva ne "Brucella" && $mlva ne "Anthracis" && $mlva ne "Coxiella"  )) {
    die "MLVA_finder currently supports only species: Burkholderia, Brucella,Anthracis, Coxiella\n";
  }


#  if($mlst){
#    if(!(checkfile($mlst_schemes,$mlst))){
#      die "your selected mlst scheme $mlst does not exist. Please select scheme from $mlst_schemes"
#    }
  #}
  if($cansnper){
    if(!(checkfile($cansnp_species,$cansnper))){
      die "your selected canSNP species $cansnper does not exist. Please select a species from $cansnp_species"
    }
  }

  if($cansnper2){
    if(!(($cansnper2 eq "bacillus_anthracis.db")| ($cansnper2 eq "francisella_tularensis.db")| ($cansnper2 eq "yersinia_pestis.db")))
    {
      die "your selected CanSNPer2 database  does not exist. Please select one of: bacillus_anthracis.db, francisella_tularensis.db, yersinia_pestis.db"
    }
  }
  if($panaroo){
    if(length($bakta)==0)
    {
      die "your selected option --panaroo enforces option bakta. Please provide --bakta <Path to Bakta DB>"
    }
  }



}

sub checkfile {
  my ($file,$word)=@_;
  my $found=0;
  open FILE, $file or die $!;#opens input file (metadate table)
      while (my $line =<FILE>) {
        chop($line); #removes whitespaces
        my @content=split(/\t/,$line);#seperates line by TAB
        if ($content[0] eq $word) {
              $found=1;
          }
      }
      return($found)
}


1;
