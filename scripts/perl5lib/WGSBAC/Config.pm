package WGSBAC::Config;
use strict;
use warnings;
use Exporter qw(import);
our @EXPORT_OK = qw(config);

##links files from metadata to inputfolder using IDs as names
#writes config files including
#samples
#pathes to input files
#pathes to folders
sub config {
  my ($config,$refstrain,$table,$resultsfolder,$binfolder,$unzipfolder,$assemblyfolder,$logfolder,$inputfolder,$tmpfolder, $benchmarkfolder,
  $envfolder,$scriptsfolder,$kraken,$snakefiles,$mlst_schemes,$cansnper,$cansnper2,$cansnp2folder,$sistr,$snippy,$parsnp,$prokka,$mincov, $minlength,$trim,$panaroo,$seqsero,$data,$sourmash,$mlva,$fasttree,$amr,$virulence,$plasmids,$platonfolder,$organism,$spi,$spifolder,$arcodb,$arcofolder,$clostox,$ctoxfolder,$confindr
  ,$ecoli,$abricatedbfolder,$mlst,$bakta,$raxml)=@_;

  print "$config in configpm\n";
  open(OUT,'>', $config) or die("Can't open current file: $!"); #opens file or gives errormessage if not possible
  print OUT "#This is the config files of WGSBAC\n";
  print OUT "#The files includes pathes used by the pipeline,settings of parameters and a list of samples\n";
  print OUT "\n";
  print OUT "#here comes general pathes for the pipeline. Feel free to adapt\n";
  print OUT "\n";

  print OUT "metadata: $table #path to metadata\n";
  print OUT "input: $inputfolder #path to input folder\n";#adds to config
  print OUT "results: $resultsfolder #path to resultfolder\n";#adds to config
  print OUT "log: $logfolder #path to logfolder\n";#adds to config
  print OUT "benchmark: $benchmarkfolder #path to benchmarking files\n";#adds to config
  print OUT "tmp: $tmpfolder #path to temp files\n";#adds to config
  print OUT "snakefiles: $snakefiles\n";#adds to config
  print OUT "envs: $envfolder #path to conda environments\n";#adds to config
  print OUT "bin: $binfolder\n";#adds to config
  print OUT "scripts: $scriptsfolder\n";#adds to config
  print OUT "kraken: $kraken #path to kraken2DB\n";#adds to config
  print OUT "sourmashDB: $sourmash #path to DB for $sourmash\n";#adds to config
  print OUT "\n";
  print OUT "#here comes parameters of the pipeline\n";

#adds Parameters to config. allows expert users to adapt tools
#non-optional
  print OUT "kraken_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  print OUT "fastqc_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  if($refstrain eq ""){
    print OUT "quast_params: \"\" #expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";

    }else
   {
    print OUT "quast_params:  -R $refstrain #reference strain. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";

   }
  print OUT "sourmash_compute_params: -p scaled=1000,k=31 #scaling and kmer size for sourmash compute expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  print OUT "sourmash_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  print OUT "fastp_params: --disable_quality_filtering --disable_adapter_trimming --disable_length_filtering --disable_trim_poly_g   #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";

  #manages which results will be integrated in multiq report
  my $report_params="";


#optional
#sets additional shovill parameters  for filtering
  my $shovill_praram="";
  if($mincov){#adds filtering
    $shovill_praram=$shovill_praram." --mincov $mincov";
  }
  if($minlength){#adds filtering
    $shovill_praram=$shovill_praram." --minlen $minlength";
  }
  if($trim){#adds trimming
    $shovill_praram=$shovill_praram." --trim";
  }
  print OUT "shovill_prarams: \"$shovill_praram\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";

  if ($sistr) {
    print OUT "sistr_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    $report_params=$report_params."--sistr";
  }
    if ($snippy) {
    print OUT "snippy_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "snippy_core_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "hierBAPS_maxdepth: 4 #max depth of hierarchical search. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "hierBAPS_npops: 50 #max number of expected  populations in data. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "hclust_maxsnp: 0,1,5,10,15,20,50,100,200,500,1000 #vector of number of snps to define a cluster using hclust. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "hclust_minmember: 2 #minimum number of isolates/samples to  a define cluster using hclust. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";
    $report_params=$report_params."--snippy";##add snpidist to report
  }
    if ($raxml) {
      print OUT "raxml_params: -m GTRGAMMA -p 2352890 #model and seed (-p) for RAxML. Expert options.You may add parameters and settings. These are directly parsed to the tool as string\n";
  }

  if (length($mlva)>0){
    $report_params=$report_params."--mlva";#add report to multiqc
    print OUT "mlvafinder_primers: $data/MLVA_finder/$mlva"."_primer.txt    #primers file for user selected species\n";
    print OUT "mlvafinder_params: -c  --predicted-PCR-size-table  #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "hclust_maxvntrs: 0,1,2,3,5,8,10,15,20 #vector of number of number of different vntrs to define a cluster using hclust. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "hclust_minmembermlv: 2 #minimum number of isolates/samples to  define a cluster using hclust. Expert options. You may change parameters and settings. These are directly parsed to the tool as string\n";

  }
  if (length($confindr)>0){
    $report_params=$report_params."--confindr";#add report to multiqc
    print OUT "confinderDB: $confindr #confinder rMLST DB \n";
    print OUT "confindr_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";


  }

  if ($ecoli){
    $report_params=$report_params."--ecolitype";
     print OUT "ecoliserotype_params: --mincov 90  #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
     print OUT "ecoliclermont_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "abricateecoli: $abricatedbfolder/ecoli.txt  #file holding donwload time stampe for ecoh and eco_Vir\n";

  }


  if ($amr){
    my $amrparams="--plus "; #finds ressitacne agains heavy metal, stres, ...
    if(length($organism)>0){
      $amrparams=$amrparams."--organism $organism"#add organism specific option
    }
    print OUT "amrfinder_params: $amrparams #expert options. --plus detects genes for strees, heavy metal resistance,. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "amr_abricate_params: \"\" #expert options for abricate AMR databases . You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "amrfinderupdate: $abricatedbfolder/amrfinder.txt  # #file holding donwload time stampe for AMRFinder\n";
    print OUT "abricateamr: $abricatedbfolder/amr.txt  #file holding donwload time stampe for card,resfinder, ncbi\n";
    $report_params=$report_params."--amr";

  }
   if ($virulence){
    $report_params=$report_params."--virulence";
     print OUT "vfdb_abricate_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "abricatevir: $abricatedbfolder/vfdb.txt  #file holding donwload time stampe for vfdb\n";

  }

   if ($plasmids){
    $report_params=$report_params."--plasmids";
     print OUT "plasmids_abricate_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
     print OUT "platon_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
     print OUT "platon_db: $platonfolder\n";#folder holding DB
    print OUT "abricateplasmidsfinder: $abricatedbfolder/plasmidfinder.txt  #file holding donwload time stampe for plasmidfinder\n";

    }



  if ($spi){
    $report_params=$report_params."--spi";
     print OUT "spiabricate_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
     print OUT "spifolder: $spifolder  #path to abricate DB \n";

  }

  if ($arcodb){
    $report_params=$report_params."--arcodb";
     print OUT "arcoabricate_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
     print OUT "arcofolder: $arcofolder  #path to abricate DB \n";

  }

  if ($clostox){
    $report_params=$report_params."--clostox";
     print OUT "clostoxabricate_params: --minid 85  #expert options. Sets min BLASTn identity. You may add parameters and settings. These are directly parsed to the tool as string\n";
     print OUT "ctoxfolder: $ctoxfolder  #path to abricate DB \n";

  }

  if ($parsnp) {
    print OUT "parsnp_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  }


  if ($panaroo) {
    print OUT "panaroo_params: \"--clean-mode strict -a core \" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    $report_params=$report_params."--panaroo";
    #adds also panaroo params as prokka is needed for panaroo
  }


  if ($prokka) {
    print OUT "prokka_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  }


  if($bakta){
    print OUT "bakta_params: -v --skip-plot #skips plot as often not working. Expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    print OUT "bakta_db: $bakta  #Complete path to bakta DB. Note: Needs to be in correct version\n";
    print OUT "prokka_params: \"\" #\n"; ##neds to be removed when prokka removed


  }
  if ($seqsero) {
    print OUT "seqsero_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
    $report_params=$report_params."--seqsero";
}

if (length($cansnper2)>0) {
  print OUT "cansnper2_db: $cansnp2folder/$cansnper2 #path to cnasnper2 DB\n";
  print OUT "cansnper2_reference: $cansnp2folder/references/$cansnper2/ #path to cansnper2 references folder\n";
  print OUT "cansnper2_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";
  $report_params=$report_params."--canSNPer2";
}

  if(length($cansnper)>0){
    print OUT "cansnperDB: $data/CanSNPerDB2017.db #DB used for CanSNPEr\n";#adds  to config
    print OUT "francisellatree: $data/francisella_tularensis_tree.txt #tree is used to add major clades (b6 and B.12 for francisella)\n";#adds  to config
    $report_params=$report_params."--cansnper";
    print OUT "cansnper_species: $cansnper #species of cansnper (-r parameter)\n";#adds  to config
    if($cansnper eq "Francisella"){ #adapts allow_differences parameter in case of francisella
      print OUT "cansnper_allow_differences: 1 #supposed to be 1 for Francisella_tularensis only\n";
    }else {
      print OUT "cansnper_allow_differences: 0\n";
    }
    print OUT "cansnper_params: \"\"#expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";

  }
  if(length($mlst)>0){
    $report_params=$report_params."--mlst";
    print OUT "mlst_file: $mlst_schemes\n";#add path of all available schems to config
    print OUT "mlst_scheme: $mlst #--scheme parameter\n";#adds selected mlst scheme to config
    print OUT "mlst_params: \"\" #expert options. You may add parameters and settings. These are directly parsed to the tool as string\n";

  }
  if($refstrain eq ""){#creates dummy refference file needed for quast.snakemake
     $refstrain=$resultsfolder."tmpref";
    system("touch $refstrain");
    }
  print OUT "refstrain: $refstrain ##path to reference strain\n";
  print OUT "report_params: \"$report_params\" #adds results included in multiqc report\n";#adds to config
  close $config;

}
#links input files into workflow of snakemake
#adds samples and fastqfiles to config
