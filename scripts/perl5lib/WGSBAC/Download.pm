package WGSBAC::Download;
use strict;
use warnings;
use Exporter qw(import);
our @EXPORT_OK = qw(download);

#downloads data
sub download {
  my ($sourmash,$spi,$spifolder,$arcodb,$arcofolder,$clostox,$ctoxfolder,$plasmids,$platonfolder,$mlst_schemes,$mlst,$amr,$virulence,$data,$ecoli,$abricatedbfolder) = @_;
  #downloads GTDB index for sourmash (if not allready there)
  unless (-e $sourmash ) {
    print "Downloading sourmash index https://osf.io/gs29b/download  -O $sourmash\n";
    system ("wget --quiet  https://osf.io/gs29b/download  -O $sourmash")
  }
  if ($plasmids){
     if (!(-e $platonfolder) ||  -M $platonfolder >30)
     { #downloads platon DB if not exist or older than 30days
    print("print Downloading platon DB from  https://zenodo.org/record/4066768/files/db.tar.gz to  $platonfolder \n");
    print("This may take a while...\n");
    system("mkdir $platonfolder");
    system("wget --quiet wget https://zenodo.org/record/4066768/files/db.tar.gz -O  $platonfolder/db.tar.gz ");
    system("tar -xzf  $platonfolder/db.tar.gz --directory $platonfolder --strip-components=1");
  }
  if (!(-e "$abricatedbfolder/plasmidfinder.txt") ||  -M "$abricatedbfolder/plasmidfinder.txt" >30) #checks if file exists or is >30days old
   { #downloads  Abricate DB for Plasmidfinder if not exist or older than 30days
    print("Snakemake will download Abricate DBs for Plasmidfinder as your folder is older than 30 days  \n");
    if (-e "$abricatedbfolder/plasmidfinder.txt"){ #removes files so that snakemake has to create it during download
      system("rm  $abricatedbfolder/plasmidfinder.txt")#removes file which will be created after download by snakemake
    }
    }

}
#download amr
if($amr){
  if (!(-e "$abricatedbfolder/amr.txt") ||  -M "$abricatedbfolder/amr.txt" >30) #checks if file exists or is >30days old
  { #downloads  Abricate DBs for CARD, NCBI, Resfinder if not exist or older than 30days
    print("Snakemake will download Abricate DBs for CARD, NCBI, Resfinderr as your folder is older than 30 days  \n");
    if (-e "$abricatedbfolder/amr.txt"){ #removes files so that snakemake has to create it during download
      system("rm  abricatedbfolder/amr.txt")#removes file which will be created after download by snakemake
    }
  }
  if (!(-e "$abricatedbfolder/amrfinder.txt") ||  -M "$abricatedbfolder/amrfinder.txt" >30) #checks if file exists or is >30days old
    { #downloads Amrfinderplus
      print("Snakemake will downloadAmrfinderplus older than 30 days  \n");
      if (-e "$abricatedbfolder/amrfinder.txt"){ #removes files so that snakemake has to create it during download
        system("rm  $abricatedbfolder/amrfinder.txt")#removes file which will be created after download by snakemake
      }
    }
  }

if($ecoli){
  if (!(-e "$abricatedbfolder/ecoli.txt") ||  -M "$abricatedbfolder/ecoli.txt" >30)
  { #downloads Abricate DBs for EcoH and ecovir  if not exist or older than 30days
    print("Snakemake will download Abricate DBs for EcoH and ecovir  as your folder is older than 30 days  \n");
    if (-e "$abricatedbfolder/ecoli.txt"){
      system("rm  $abricatedbfolder/ecoli.txt")#removes file which will be created after download by snakemake
    }
    }
  }


  if($virulence){
    if (!(-e "$abricatedbfolder/vfdb.txt") ||  -M "$abricatedbfolder/vfdb.txt" >30)
    { #downloads Abricate DB for VFDB   if not exist or older than 30days
      print("Snakemake will download Abricate DB for VFDB as your folder is older than 30 days  \n");
      if (-e "$abricatedbfolder/vfdb.txt"){
        system("rm  $abricatedbfolder/vfdb.txt")#removes file which will be created after download by snakemake
      }
      }
    }

  if ($mlst){
     if (!(-e $mlst_schemes) ||  -M $mlst_schemes >30)
     { #downloads mlst_schemes  if not exist or older than 30days
    print("Snakemake will download MLST-Schemes as your schemes are older than 30 days  \n");
    if (-e $mlst_schemes ){
      system("rm  $mlst_schemes")
    }
  }
  }



  #download Salmonella pathogenicity islands
  if ($spi){
    unless (-e $spifolder){
      print "Cloning from Salmonella pathogenicity islands at GIT https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database  $spifolder\n";
      system("git clone --quiet https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database  $spifolder")

    } if (-M $spifolder >30) {
      print "SPI data older then 30 days. Pulling from GIT to update  Salmonella pathogenicity islands   https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database  $spifolder\n";
      system("git -C $spifolder pull --quiet https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database")

    }
  }

  if ($arcodb){
    unless (-e $arcofolder){
      print "Cloning from Aliarcobacter butzleri - AMR and virulence database at GIT https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri  $spifolder\n";
      system("git clone --quiet  https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri  $arcofolder")

  } if (-M $arcofolder >30)  {
    print "Aliarcobacter butzleri data older than 30 days. Pulling from GIT to update  Aliarcobacter butzleri - AMR and virulence database  https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri  $arcofolder\n";
    system("git -C $arcofolder pull  --quiet https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri")

  }
  }

  if ($clostox){
    unless (-e $ctoxfolder){
      print "Cloning from Clostridium perfringens toxinotypes at  GIT https://gitlab.com/FLI_Bioinfo_pub/perfringens-toxinotypes $ctoxfolder\n";
      system("git clone --quiet https://gitlab.com/FLI_Bioinfo_pub/perfringens-toxinotypes  $ctoxfolder")

  } if (-M $ctoxfolder >30)  {
    print "Clostridium perfringens toxinotypes data older than 30 days. Pulling from GIT to update  Clostridium perfringens toxinotypes DB https://gitlab.com/FLI_Bioinfo_pub/perfringens-toxinotypes  $ctoxfolder\n";
    system("git -C $ctoxfolder pull --quiet https://gitlab.com/FLI_Bioinfo_pub/perfringens-toxinotypes")

  }
  }

}
1;
