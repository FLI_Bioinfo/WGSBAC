package WGSBAC::Initfolders;
use strict;
use warnings;
use Exporter qw(import);
our @EXPORT_OK = qw(initfolders);

#creates fodlers for results input and tmp
#creates variables for subfolders within these folders
#you may change the folder structure here
sub initfolders {
  my ($resultsfolder,$binfolder) = @_;
  my $assemblyfolder=$resultsfolder."finalAssembly/";;#assembly folder holding given and self-assembled files in format ID.fasta
  my $unzipfolder=$resultsfolder."fastq/";#folder holding unzipped fastq files
  my $logfolder=$resultsfolder."log/";#path to log directory
  my $inputfolder=$resultsfolder."input/"; #input folder including raw fastq files in format ID.R{1/2}.fastq.gz
  my $tmpfolder=$resultsfolder."tmp/"; #tmp folder including tmp files
  my $benchmarkfolder=$resultsfolder."benchmark/"; #benchmarking folder
  my $envfolder=$binfolder."envs/";#folder holding the envs
  my $scriptsfolder=$binfolder."scripts/";#folder holding the  scripts
  my $snakefiles=$binfolder."snakefiles/";#folder holding the envs
  my $config=$resultsfolder."config.yaml";#config file for snakemake
  my $conda=$binfolder."conda/";#folder holding software downloaded from conda
  if (!(-e $resultsfolder)) { #creates results folder if not exists
    mkdir($resultsfolder);
  }
  if (!(-e $inputfolder)) { #creates input folder if not exists
    mkdir($inputfolder);
  }
  if (!(-e $tmpfolder)) { #creates input folder if not exists
    mkdir($tmpfolder);
  }
  return ($unzipfolder,$assemblyfolder,$logfolder,$inputfolder,$tmpfolder, $benchmarkfolder,$envfolder,$scriptsfolder,$snakefiles,$config,$conda);
}
1;
