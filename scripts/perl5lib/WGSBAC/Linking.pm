package WGSBAC::Linking;
use strict;
use warnings;
use Exporter qw(import);
our @EXPORT_OK = qw(link_samples);


sub link_samples {
  my ($table,$unzipfolder,$assemblyfolder,$inputfolder)=@_;
  my ($fastq1, $fastq2,$fasta)="";
  my $ref="";
  my @fastqs;#array holding all ID of fastq files. Will be added to confing
  my $nrline=0;#counts number of lines from input to give error message
  open FILE, "$table" or die $!;#opens input file (metadate table)
  while (my $line=<FILE>) { #reads table per line
    $nrline++;#counts number of lines from input to give error message
    next if $nrline==1;#skips header line
    chop($line); #removes whitespaces
    my @content=split(/\t/,$line);#seperates line by TAB
    if(!($content[3] eq "")) { #fourth column is non empty-> we have fastqfiles
      if ($content[3] =~ /\.fastq.gz$/i || $content[3] =~ /\.fq.gz$/i ) {#gzipped fastq file
      $fastq1="$inputfolder"."$content[0]_R1.fastq.gz";#link namke of R1
      $fastq2="$inputfolder"."$content[0]_R2.fastq.gz";#link namke of R2
      if (!(-e $fastq1)) { #creates links  if not allready exist
        symlink($content[3],$fastq1);
        symlink($content[4],$fastq2);
      }
    }
    elsif($content[3] =~ /\.fastq$/i || $content[3] =~ /\.fq$/i){#unzipped fastq file
      mkdir($unzipfolder);
      $fastq1=$unzipfolder."$content[0]_R1.fastq";#link namke of R1
      $fastq2=$unzipfolder."$content[0]_R2.fastq";#link namke of R2
      if (!(-e $fastq1)) { #creates links  if not exist
        symlink($content[3], $fastq1);
        symlink($content[4] ,$fastq2);
      }
    }
    push  @fastqs, $content[0];
    }
    elsif(!($content[5] eq "")){##sixth column is non empty-> we have fastafiles
      $fasta="$assemblyfolder"."$content[0].fasta";#filename for genome assembly
      mkdir($assemblyfolder);#creates subdirectory for linking the assembly
      if (!(-e $fasta)) { #creates links  if not exist
        symlink($content[5] ,$fasta); #creates link
      }
      if($content[2] eq "reference"){#checks for ref strain
        $ref=$fasta;#file for genome assembly
      #  system("ln -s $content[5] $refstrain"); #creates link
        }
    }

  }
  

  return ($ref)

  }

1,
