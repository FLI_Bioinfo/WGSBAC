package WGSBAC::Snakefile;
use strict;
use warnings;
use File::Copy;
use Exporter qw(import);
our @EXPORT_OK = qw(snakefile);

#copies master.snakefile template to user-specific snakefile
#adds to the rule_all the results the user wants
sub snakefile {
  my ($sistr,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$seqsero,$binfolder,$mlva,$amr,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$raxml)= @_;
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();#get time and date
  $year=$year+1900;#add to  make year mroe than 200
  $mon=$mon+1;#normal range ist from 0 to 11
  my $username = $ENV{USER};
  my $snakefile="snakefile_$username"."_$year"."_$mon"."_$mday"."_$hour:$min".".Snakefile";#add time stamp
  my $itol="";#if any phyl tree will be produced, produce also itol file
  if($parsnp|| $snippy||$panaroo||$mlst||$mlva){
    $itol=1;
  }
  unless (-e $binfolder."tmpsnakefiles") {
    mkdir($binfolder."tmpsnakefiles");
  }
    $snakefile=$binfolder."tmpsnakefiles/".$snakefile;
   copy($binfolder."/snakefiles/master.Snakefile",$snakefile) or die "Copy failed: $!";#create a copy with time stamp
  include_statements($snakefile,$itol,$sistr,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$seqsero,$mlva,$amr,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$raxml);
  rule_all($snakefile,$itol,$sistr,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$seqsero,$mlva,$amr,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$raxml);




  return($snakefile);

}
#helper, which adds include statements to snakefile
sub include_statements {
  my ($snakefile,$itol,$sistr,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$seqsero,$mlva,$amr,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$raxml)=@_;
  open(SF, '>>', $snakefile) or die; #open snakefile to APPEND
  print SF "\n";
  print SF "\n";
  print SF "\n";
  print SF "#optional tools\n";

  if($cansnper){
    print SF "include: snakefiles + \"cansnper.Snakefile\"\n";
  }
  if($raxml){
    print SF "include: snakefiles + \"raxml.smk\"\n";
  }

  if($cansnper2){
    print SF "include: snakefiles + \"cansnper2.Snakefile\"\n";
  }

  if(length($confindr)>0){
    print SF "include: snakefiles + \"confindr.smk\"\n";
  }

  if($ecoli){
  print SF "include: snakefiles + \"serotypefinder.smk\"\n";#ecoli serotype
  print SF "include: snakefiles + \"clermont.smk\"\n";#Ecoli Clermont phylogrup
}

  if($amr){
    print SF "include: snakefiles + \"amr.Snakefile\"\n";
  }
  if($virulence ){
    print SF "include: snakefiles + \"virulence.Snakefile\"\n";
  }
  if($spi ){
    print SF "include: snakefiles + \"spi.Snakefile\"\n";
  }

  if($plasmids){
    print SF "include: snakefiles + \"plasmids.Snakefile\"\n";
  }
  if($clostox){
    print SF "include: snakefiles + \"clostox.Snakefile\"\n";
  }
  if($arcodb){
    print SF "include: snakefiles + \"arcodb.Snakefile\"\n";
  }
  if($panaroo){
    print SF "include: snakefiles + \"panaroo.smk\"\n";
  }
  if($prokka){
    print SF "include: snakefiles + \"annotation.smk\"\n";
  }
  if($bakta){
    print SF "include: snakefiles + \"annotation.smk\"\n";
  }
    if($parsnp){
    print SF "include: snakefiles + \"parsnp.Snakefile\"\n";
  }


  if("$mlst"){
    print SF "include: snakefiles + \"mlst.Snakefile\"\n";

  }
  if("$sistr"){
    print SF "include: snakefiles + \"sistr.Snakefile\"\n";
  }

  if ($itol){
    print SF "include: snakefiles + \"itol.Snakefile\"\n";
  }
  if ($snippy){
    print SF "include: snakefiles + \"snippy.Snakefile\"\n";
  }
  if($seqsero){
  print SF "include: snakefiles + \"seqSero.Snakefile\"\n";
}
  if(length($mlva)>0){
    print SF "include: snakefiles + \"mlvafinder.smk\"\n";
  }


  close $snakefile;

}

#helper, which adds rule all to snakefile
#adds optional taks dependent on user selection
sub rule_all {
  my ($snakefile,$itol,$sistr,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$seqsero,$mlva,$amr,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$raxml)=@_;
  open(SF, '>>', $snakefile) or die; #open snakefile to APPEND
  print SF "\n";
  print SF "\n";
  print SF "\n";
  print SF "#rule_all defines all taks snakemake will compute\n";
  print SF "#obligatory tasks\n";
  print SF "rule all:\n";
  print SF "  input:\n";
  print SF "        multiqc_fastqc,\n";
  print SF "        quast,\n";
  print SF "        q30,\n";
  print SF "        kraken_reads_species,\n";
  print SF "        kraken_contigs_species,\n";
  print SF "        kraken_reads_genera,\n";
  print SF "        coverage,\n";
  print SF "        multiqc_rep,\n";
  print SF "        qc,\n";
  print SF "#optional  tasks\n";


  if ($amr) {
    print SF "        amrfinder,\n";
    print SF "        abricate_card,\n";
    print SF "        abricate_resfinder,\n";
    print SF "        abricate_ncbi,\n";

  }

  if(length($confindr)>0){
    print SF "        confindr,\n";

    }
  if ($sistr) {
    print SF "        sistr,\n";
  }

  if ($ecoli) {
  print SF "        ecoh,\n";
  print SF "        ecoliclermont,\n";

}

  if ($mlst) {
    print SF "        mlst,\n";
    print SF "        itolmlst,\n";

  }
  if ($cansnper) {
    print SF "        cansnper,\n";
  }
  if ($cansnper2) {
    print SF "        cansnper2,\n";
  }


  if ($panaroo) {
    print SF "        panaroo,\n";
  if($raxml){
    print SF "        raxml_panaroo,\n";

    }
  }
  if ($snippy) {
    print SF "        snippy_dist,\n";
    print SF "        snpclust,\n";#itolclust
    print SF "        itolclustsnps,\n";
    if($raxml){
      print SF "        raxml_snippy,\n";

      }

  }
  if ($plasmids){
     print SF "        abricate_plasmidfinder,\n";
     print SF "        platon,\n";
  }

  if ($virulence){
     print SF "        abricate_vfdb,\n";
  }

   if ($spi){
     print SF "        abricate_spi,\n";
  }

   if ($clostox){
     print SF "        abricate_clostox,\n";
  }

  if ($arcodb){
     print SF "        abricate_arcodbamr,\n";
     print SF "        abricate_arcodbvir,\n";
  }




  if ($parsnp) {
     print SF "        parsnp,\n";
  }
  if ($prokka) {
     print SF "        prokka,\n";
  }
  if ($bakta) {
     print SF "        bakta,\n";
  }
  if ($itol) {
     print SF "        itol,\n";
  }
  if ($seqsero) {
     print SF "        seqSero,\n";
  }
  if ($mlva) {
     print SF "        mlva,\n";
     print SF "        itolclustmlva,\n";

  }
  close $snakefile;
}

1;
