


"""
Download AMRfinderDB

"""

rule downloadamrfinder:
    output:#directory holding all mlst results for each sample
        db=config["amrfinderupdate"],
    log:
        amr_log+"downloadamrfinder.log"
    benchmark:
        amr_bench+"downloadamrfinder.txt"
    conda:
        envs+ "amrfinder.yaml"
    shell:
        " amrfinder -u   1> {output.db} && chmod 777 {output.db}  2>{log}"




"""
Uses AMRfinderplus to detect AMR genes

"""

rule amrfinderplus:
    input:#input is final assemblies after spades and filtering with coverage and length
        fasta=renaming_res+"{sample}.fasta",
        db=config["amrfinderupdate"],
    output:#directory holding all mlst results for each sample
        amrfinder_res=amr_res+"{sample}.csv",
    log:
        amr_log+"amrfinder.{sample}.log"
    benchmark:
        amr_bench+"amrfinder.{sample}.txt"
    conda:
        envs+ "amrfinder.yaml"
    threads:
        4
    params:
        additional=config["amrfinder_params"]
    shell:
        " amrfinder --threads {threads} -n {input.fasta} {params.additional}  1> {output.amrfinder_res} 2>{log} || touch {output.amrfinder_res}"



rule collect_amrfinderplus:
    input:#input is
        amrfiles=expand(amr_res+"{sample}.csv", sample=SAMPLES),
        metadata=config['metadata'],
    output:#outpus as csv and xls
        allcsv=amr_res+"allAmrfinder.csv",
        allxls=amr_res+"allAmrfinder.xlsx",
        multiqc=amr_res+"AMRfinder_mqc.txt",
    log:
        amr_log+"collect_amrfinderplus.log"
    benchmark:
        amr_bench+"collect_amrfinderplus.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineamrfinder.R"#converts to xls

#downlads CARD, resfinder, ncbi if older than 30 days
rule amrabricatedownload:
    output:#directory holding all mlst results for each sample
        db=config["abricateamr"],
    log:
        amr_log+"downloadabricateamr.log"
    benchmark:
        amr_bench+"downloadabricateamr.txt"
    conda:
        envs+ "abricate.yaml"
    shell:
        " abricate-get_db --db card --force  2>{log}&&\
	abricate-get_db --db ncbi --force  2>>{log}&&\
	abricate-get_db --db resfinder --force  2>>{log}&&\
	touch {output.db} && chmod 777 {output.db}   2>>{log}"




"""
Runs Abricate using assembly as input against following databases:
-resfinder
-CARD
-VFDB
"""
rule abricate_AMR:
    input:
        fasta=renaming_res+"{sample}.fasta",
	db=config["abricateamr"],
    output:
        resfinder=amr_res+"{sample}_resfinder.csv",
        card=amr_res+"{sample}_card.csv",
        ncbi=amr_res+"{sample}_ncbi.csv",
    log:
        resfinder=amr_log+"{sample}_resfinder.log",
        card=amr_log+"{sample}_card.log",
        ncbi=amr_log+"{sample}_ncbi.log",
    threads: 2
    benchmark:
        amr_bench+"abricate.{sample}.txt"
    params:
        additional=config["amr_abricate_params"]
    conda:
      envs + "abricate.yaml"
    shell:
      "(abricate --threads {threads} {input.fasta}  --db resfinder  {params.additional} >{output.resfinder} 2>>{log.resfinder} || touch {output.resfinder})  &&\
      (abricate --threads {threads} {input.fasta}  --db card  {params.additional} >{output.card} 2>>{log.card} || touch {output.card} ) &&\
      (abricate --threads {threads} {input.fasta}  --db ncbi  {params.additional} >{output.ncbi} 2>>{log.ncbi} || touch {output.card})"


"""
Combines/summarizes Abricate results in one single CSV
"""
rule summary_AMR:
    input:
        resfinder=expand(amr_res+"{sample}_resfinder.csv", sample=SAMPLES),
        card=expand(amr_res+"{sample}_card.csv", sample=SAMPLES),
        ncbi=expand(amr_res+"{sample}_ncbi.csv", sample=SAMPLES),
    output:
        resfinder_csv=temp(abricate+"resfinder.csv"),
        ncbi_csv=temp(abricate+"ncbi.csv"),
        card_csv=temp(abricate+"card.csv"),
    log:
        resfinder=amr_log+"combine_amr_resfinder.log",
        ncbi=amr_log+"combine_abricate_.log",
        card=amr_log+"combine_abricate_card.log",
    benchmark:
        amr_bench+"combine_abricate.txt"
    conda:
      envs + "abricate.yaml"
    shell:
        "abricate  --summary  {input.resfinder}   >{output.resfinder_csv} 2>>{log.resfinder}&&\
        abricate  --summary  {input.card}   >{output.card_csv} 2>>{log.card}&&\
        abricate  --summary  {input.ncbi}   >{output.ncbi_csv} 2>>{log.ncbi}"



"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
"""
rule collect_abbricate_ncbi:
    input:
        abricateres=abricate+"ncbi.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=amr_res+"all_ncbi.csv",#output csv format
        abricate_xls=amr_res+"all_ncbi.xls",#output XLS format
        abricate_mqc=amr_res+"Abricate_ncbi_mqc.txt",#output MQC format
    params:
        db="ncbi"
    log:
        amr_log+"collect_abbricate_ncbi.log"
    benchmark:
        amr_bench+"collect_abbricate_ncbi.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls

"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
"""
rule collect_abbricate_card:
    input:
        abricateres=abricate+"card.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=amr_res+"all_card.csv",#output csv format
        abricate_xls=amr_res+"all_card.xls",#output XLS format
        abricate_mqc=amr_res+"abricate_card_mqc.txt",#output MQC format
    params:
        db="card"
    log:
        amr_log+"collect_abbricate_card.log"
    benchmark:
        amr_bench+"collect_abbricate_card.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls








"""
Converts combined abbricate resfinder results in XLS and MQC report
"""
rule collect_abbricate_resfinder:
    input:#input is
        abricateres=abricate+"resfinder.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=amr_res+"all_resfinder.csv",#output csv format
        abricate_xls=amr_res+"all_resfinder.xls",#output XLS format
        abricate_mqc=amr_res+"Abricate_Resfinder_mqc.txt",#output MQC format
    params:
        db="resfinder"
    log:
        amr_log+"collect_abbricate.log"
    benchmark:
        amr_bench+"collect_resfinder.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls
