
"""
Prokka for gene prediction and annotation

"""
rule prokka:
    input:#input is final assemblies after spades and filtering with coverage and length
        fasta=renaming_res+"{sample}.fasta"
    output:#directory holding all prokka results for each sample
        gff=prokka_res + "{sample}/{sample}.gff"
    threads:
        4
    benchmark:
        prokka_bench+"{sample}.txt"
    log:
        prokka_log+"{sample}.log"
    conda:
        envs +"prokka.yaml"
    params:
        additional=config["prokka_params"],
        prefix="{sample}",
        strain_name="{sample}",
        locustag= "{sample}",
        outdir=prokka_res+"{sample}/"
    shell:
        "prokka --cpus {threads}  --force --kingdom Bacteria --gcode 11\
            --outdir {params.outdir}\
            --prefix {params.prefix} --locustag {params.locustag} --strain {params.strain_name} {params.additional} \
            {input.fasta} &> {log} || true"



"""
Bakta for gene prediction and annotation

"""
rule bakta:
    input:#input is final assemblies after spades and filtering with coverage and length
        fasta=renaming_res+"{sample}.fasta"
    output:#directory holding all bakta results for each sample
        folder=directory(bakta_res + "{sample}/"),
        gff=bakta_res + "{sample}/{sample}.gff3"
    threads:
        4
    benchmark:
        bakta_bench+"{sample}.txt"
    log:
        bakta_log+"{sample}.log"
    conda:
        envs +"bakta.yaml"
    params:
        additional=config["bakta_params"],
        db=config["bakta_db"],
    shell:
        "bakta --output {output.folder} --db {params.db} \
         -t {threads}  {params.additional}\
           {input.fasta} &> {log} || true"
