
#abricate for Arcobacter Virulence and AMR
rule abricate_arcodb:
    input:
        fasta=renaming_res+"{sample}.fasta",
    output:
        amr=arco_res+"{sample}_arcodbamr.csv",
        vir=arco_res+"{sample}_arcodbvir.csv",
    log:
        amr=arco_log+"{sample}_arcoamr.log",
        vir=arco_log+"{sample}_arcovir.log",
    threads: 1
    benchmark:
        arco_bench+"{sample}.txt"
    params:
        additional=config["arcoabricate_params"],
        datadir=config["arcofolder"],#path to arcodb abricate dab
    conda:
      envs + "abricate.yaml"
    shell:
      "(abricate --threads {threads} {input.fasta}  --db ARCO_IBIZ_AMR  --datadir {params.datadir}  {params.additional} >{output.amr} 2>>{log.amr}) || touch {output.amr}  &&\
      (abricate --threads {threads} {input.fasta}  --db ARCO_IBIZ_VIRULENCE  --datadir {params.datadir}  {params.additional} >{output.vir} 2>>{log.vir}) || touch {output.vir}"

"""
Combines/summarizes Abricate results in one single CSV
"""
rule summary_arcodb:
    input:
        arcodbamr=expand(arco_res+"{sample}_arcodbamr.csv", sample=SAMPLES),
        arcodbvir=expand(arco_res+"{sample}_arcodbvir.csv", sample=SAMPLES),
    output:
        amr=temp(abricate+"arcodbamr.csv"),
        vir=temp(abricate+"arcodbvir.csv"),
    log:
        amr=arco_log+"combine_abricate_arcodbamr.log",
        vir=arco_log+"combine_abricate_vir.log",
    benchmark:
        arco_bench+"combine_abricate_arcodb.txt"
    conda:
      envs + "abricate.yaml"
    shell:
      "abricate  --summary  {input.arcodbamr}   >{output.amr} 2>>{log.amr} &&\
      abricate  --summary  {input.arcodbvir}   >{output.vir} 2>>{log.vir}"

rule collect_abbricate_arcodbamr:
    input:#input is
        abricateres=abricate+"arcodbamr.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=arco_res+"all_arcodbamr.csv",#output csv format
        abricate_xls=arco_res+"all_arcodbamr.xls",#output XLS format
        abricate_mqc=arco_res+"abricate_arcodbamr_mqc.txt",#output MQC format
    params:
        db="arcodbamr"
    log:
        arco_log+"collect_abbricate.log"
    benchmark:
        arco_bench+"collect_abbricate.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls


rule collect_abbricate_arcodbvir:
    input:#input is
        abricateres=abricate+"arcodbvir.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=arco_res+"all_arcodbvir.csv",#output csv format
        abricate_xls=arco_res+"all_arcodbvir.xls",#output XLS format
        abricate_mqc=arco_res+"abricate_arcodbvir_mqc.txt",#output MQC format
    params:
        db="arcodbvir"
    log:
        arco_log+"collect_abbricate.log"
    benchmark:
        arco_bench+"collect_abbricate.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls
