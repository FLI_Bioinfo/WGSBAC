"""
Prepares a folder holding links to all assemblies as input for downstream tools (e.g. parsnp, ksnp,...)
"""
rule prepare_Assemblyfolder:
    input:
        # Input are all (own,given) assemblies
        contigs=expand(renaming_res+"{sample}.fasta", sample=SAMPLES)
    output:
        #  directory for parsnp. deleted after end of pipeline
        assembly=temp(directory(assemblies_tmp))
    benchmark:
        benchmark+"prepare_Assemblyfolder.txt"
    log:
        log+"prepare_Assemblyfolder.log"
    run:
        if not os.path.exists(output.assembly):
            os.makedirs(output.assembly, exist_ok=True)
        for contig in input.contigs:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(contig)
            head, tail = os.path.split(contig) #warning; does not work if contig ends with / or \
            outfile = str(output.assembly)+"/"+tail
            shell("if [  -s {abspath}  ]; then ln -s {abspath} {outfile} >{log} 2>{log}; fi")
"""
Prepares a folder holding all assembly files where name of assembly files is replaced with common name
useful for Ridom and others
"""
rule rename_assemblyfiles:
    input:
        # Input are all (own,given) assemblies
        assemblies=expand(renaming_res+"{sample}.fasta", sample=SAMPLES),
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
    output:
        #  directory holding renamed contigs
        assemblyfolder=directory(assemblies_renamed)
    benchmark:
        benchmark+"rename_assemblyfiles.txt"
    log:
        log+"rename_assemblyfiles.log"
    conda:
        envs + "rxls.yaml"
    script:
        "{scripts}renameAssemblyfiles.R"
