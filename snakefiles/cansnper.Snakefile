#

"""
Run CanSNPer on each assembly of fastq files.
NOTE -r Francisella --allow_differences 1 are Francisella specific parameters
"""
rule CanSNPer:
    input:#input is final assemblies
        fasta=renaming_res+"{sample}.fasta",
        db=config["cansnperDB"]
    output:#result from cansnper
        subclade=cansnper_res+"{sample}.fasta_subclade.txt",
    threads:
        1
    conda:
        envs + "cansnper.yaml"
    log:
        cansnper_log+"{sample}.log"
    benchmark:
        cansnper_bench+"{sample}.txt"
    params:
        reference=config["cansnper_species"],
        allow_differences=config["cansnper_allow_differences"],
        additional=config["cansnper_params"]
    shell:##allow differences is needed for francisella only, otherwise results are wrong
        "CanSNPer -i   {input.fasta} -n {threads} -r {params.reference} --allow_differences {params.allow_differences} -tv -b  {input.db} {params.additional} 1>{output.subclade} 2>{log} || touch {output.subclade} "





"""
Collects CanSNPer on each assembly (own produced assemblies and from given genomes) into one table (CSV and XLS)
output is xls, csv and inout for mutliqc
"""
rule collect_CanSNPer:
    input:#input is
        resown=expand(cansnper_res+"{sample}.fasta_subclade.txt", sample=SAMPLES),
        metadata=config['metadata'],
        clades=config["francisellatree"]
    output:#outpus as csv and xls
        allcsv=cansnper_res+"allcanSNPer.csv",
        allxls=cansnper_res+"allcanSNPer.xlsx",
        multiqc=cansnper_res+"CanSNPer_Subclades_mqc.txt",
    log:
        cansnper_log+"allcansper.log"
    params:
        reference=config["cansnper_species"],
    benchmark:
        cansnper_bench+"allcansper.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineCanSNPer.R"#converts to xls
