
"""
Run CanSNPer2  on each assembly of fastq files.
"""
rule CanSNPer2:
    input:#input is final assemblies
        fasta=renaming_res+"{sample}.fasta",
    output:#result from cansnper
        outdir=directory(cansnper2_res+"{sample}"),
    conda:
        envs + "cansnper2.yaml"
    log:
        cansnper2_log+"{sample}cansnper2.log"
    benchmark:
        cansnper2_bench+"{sample}cansnper2.txt"
    params:
        reference=config["cansnper2_reference"],
        db=config["cansnper2_db"],
        additional=config["cansnper2_params"],
        tmpdir=cansnper2_tmp+"{sample}"
    shell:##allow differences is needed for francisella only, otherwise results are wrong
        "CanSNPer2    --refdir {params.reference} -o {output.outdir} --tmpdir {params.tmpdir}  --database {params.db} --verbose {params.additional}  {input.fasta}  2>{log} || mkdir {output.outdir}"


"""
Collects CanSNPe2r on each assembly
"""
rule collect_CanSNPer2:
    input:#input is
        resown=expand(cansnper2_res+"{sample}", sample=SAMPLES),
        metadata=config['metadata'],
    output:#outpus as csv and xls
        allcsv=cansnper2_res+"allcanSNPer2.csv",
        allxls=cansnper2_res+"allcanSNPer2.xlsx",
        multiqc=cansnper2_res+"CanSNPer2_Subclades_mqc2.txt",
    log:
        cansnper2_log+"allcansper2.log"
    benchmark:
        cansnper2_bench+"allcansper2.txt"
    params:
        db=config["cansnper2_db"],
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineCanSNPer2.R"#converts to xls
