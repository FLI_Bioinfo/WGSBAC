

"""
Clermont phylogrup identification with EZClermont

"""

#input is final assemblies after spades and filtering with coverage and length
#directory holding all mlst results for each sample

rule clermont:
    input:
        fasta=renaming_res+"{sample}.fasta",
    output:
        ecolitype_res+"{sample}.clermont.tsv",
    log:
        ecolitype_log+"{sample}_clermont.log"
    benchmark:
        ecolitype_bench+"{sample}_bench.txt"
    conda:
        envs+"clermont.yaml"
    params:
      additional=config["ecoliclermont_params"],
    threads: 1
    shell:
        "(ezclermont {input} {params.additional} 1>{output} 2>{log}) || touch {output}"



rule clermont_summary:
	input:
		expand(ecolitype_res+"{sample}.clermont.tsv", sample=SAMPLES)
	output:
		sum=ecolitype_res+"ezclermont_summary.tsv",
		sumxls=ecolitype_res+"ezclermont_summary.xls",
		mqc=ecolitype_res+"Clermont_mqc.txt"
	log:
		ecolitype_log+"clermont_summary.log"
	script:
	    "../scripts/combine_ezclermont.R"
