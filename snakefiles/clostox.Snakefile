
#abricate for Clostridia Toxin Typing
rule abricate_clostox:
    input:
        fasta=renaming_res+"{sample}.fasta",
    output:
        clostox_res+"{sample}_clostox.csv",
    log:
        clostox_log+"{sample}_clostox.log",
    threads: 1
    benchmark:
        clostox_bench+"{sample}.txt"
    params:
        additional=config["clostoxabricate_params"],
        datadir=config["ctoxfolder"],#path to clostox abricate dab
    conda:
      envs + "abricate.yaml"
    shell:
      "(abricate --threads {threads} {input.fasta}  --db abricate_db --datadir {params.datadir}  {params.additional} >{output} 2>>{log}) || touch {output}"

"""
Combines/summarizes Abricate results in one single CSV
"""

rule collect_abbricate_clostox:
    input:#input is
        clostox=expand(clostox_res+"{sample}_clostox.csv", sample=SAMPLES),
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=clostox_res+"all_clostox.csv",#output csv format
        abricate_xls=clostox_res+"all_clostox.xls",#output XLS format
        abricate_mqc=clostox_res+"abricate_clostox_mqc.txt",#output MQC format
    params:
        db="clostox"
    log:
        clostox_log+"collect_abbricate.log"
    benchmark:
        clostox_bench+"collect_abbricate.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_clostox.R"#converts to xls
