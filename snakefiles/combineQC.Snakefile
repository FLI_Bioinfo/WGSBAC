
"""
Combines a minimal informaitve set of QC information
"""
rule combineQC:
    input:
        metadata=config['metadata'],
        q30=fastp_res+"allq30.csv",#q30 results calculated by fastp
        coverage=coverage_res+"allcoverage.csv",
	krakensp=kraken_reads+"kraken_reads_per_species.csv",#final result as csv
	krakeng=kraken_reads+"kraken_reads_per_genera.csv",#final result as csv
	quast=quast_res+"allquast.csv",
    output:#results in csv and xls
        allcsv=results+"combinedQC.csv",
        allxls=results+"combinedQC.xls",
    log:
       log+"combineQC.log"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineQC.R"#converts to xls
