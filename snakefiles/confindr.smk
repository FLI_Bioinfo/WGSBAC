

"""
Runs condindr    detect contamination in bacterial NGS data, both between and within species.
https://olc-bioinformatics.github.io/ConFindr/
"""


#prepares a folder with links to all fastq files
rule prepare_fastqfolder:
    input:#unzipped fastq files as input
        fw= expand(unzip_res+"{fastq}_R1.fastq", fastq=FASTQ),
        rv= expand(unzip_res+"{fastq}_R2.fastq",fastq=FASTQ),
    output:
        #  directory for confindr
        fastqfolder=temp(directory(fastq_tmp))
    benchmark:
        benchmark+"prepare_fastqfolder.txt"
    log:
        log+"prepare_fastqfolder.log"
    run:
        if not os.path.exists(output.fastqfolder):
            os.makedirs(output.fastqfolder, exist_ok=True)
        for fw in input.fw:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(fw)
            head, tail = os.path.split(fw) #warning; does not work if contig ends with / or \
            outfile = str(output.fastqfolder)+"/"+tail
            shell("if [  -s {abspath}  ]; then ln -s {abspath} {outfile} >{log} 2>{log}; fi")
        for rv in input.rv:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(rv)
            head, tail = os.path.split(rv) #warning; does not work if contig ends with / or \
            outfile = str(output.fastqfolder)+"/"+tail
            shell("if [  -s {abspath}  ]; then ln -s {abspath} {outfile} >{log} 2>{log}; fi")



#runs confindr on assembly folder
rule confindr:
    input:#input is fastq folder
        fastq_tmp=fastq_tmp,
    output:#directory holding all confindr results
        outfolder=directory(confindr_res),
    log:
        confindr_log+"confindr.log"
    benchmark:
        confindr_bench+"confindr.txt"
    conda:
        envs+ "confindr.yaml"
    threads:
        16
    params:
        rmlstDB=config["confinderDB"],#selected scheme from user
        additional=config["confindr_params"],
    shell:
        "confindr.py -t {threads}  -i {input.fastq_tmp}  -o {output.outfolder}  --rmlst -d {params.rmlstDB} {params.additional}   2>{log} || mkdir {output.outfolder}"



"""
Converts confindr into XLS and MQC report
"""
rule collect_confindr:
    input:#input is
        localres= confindr_res,
        metadata=config['metadata'],
    output:#outpus as csv and xls
        allcsv=results+"allConfindR.csv",
        allxls=results+"allConfindR.xlsx",
        multiqc=results+"ConFindR_mqc.txt",
    log:
        confindr_log+"collect_confindr.log"
    benchmark:
        confindr_bench+"collect_confindr.txt"
    conda:
        envs+ "rxls.yaml"
    script:
        "../scripts/combineconfindr.R"#converts to xls
