

"""
Classification of reads using Kraken.
"""
rule kraken_reads:
    input:#input is raw reads
        fw=unzip_res+"{fastq}_R1.fastq",
        rv=unzip_res+"{fastq}_R2.fastq",
        db=config["kraken"]
    output:
        results=kraken_reads+"{fastq}/all.kraken",#all
        report=kraken_reads+"{fastq}/{fastq}.report.txt",#
    threads:
        8
    conda:
        envs + "kraken2.yaml"
    params:
        additional=config["kraken_params"]
    log:
        kraken_log+"{fastq}_reads.log"
    benchmark:
        kraken_bench+"{fastq}_reads.txt"
    shell:
        "kraken2 -db {input.db} \
            --threads {threads} \
            --paired \
            --output {output.results} \
            --report {output.report} {params.additional}\
            {input.fw} {input.rv} &>  {log}"

"""
Classification of contigs using Kraken.
"""
rule kraken_contigs:
    input:#input is final assemblies after spades and filtering with coverage and length AND kraken DB
        contig=renaming_res+"{sample}.fasta",
        db=config["kraken"]
    output:
        results=kraken_contigs+"{sample}/all.kraken",#all
        report=kraken_contigs+"{sample}/{sample}.report.txt"#all
    threads:
        8
    params:
        additional=config["kraken_params"]
    conda:
        envs + "kraken2.yaml"
    log:
        kraken_log+"{sample}_contigs.log"
    benchmark:
        kraken_bench+"{sample}_contigs.txt"
    shell:
        "(kraken2 -db {input.db} \
            --threads {threads} \
            --output {output.results} \
            --report {output.report} {params.additional}\
            {input.contig} &>  {log}) || (touch {output.results}; touch {output.report}) "

"""
create kraken species reads  report
"""
rule kraken_report_reads_sepcies:
    input:
        localres=expand(kraken_reads+"{fastq}/{fastq}.report.txt",fastq=FASTQ),
        metadata=config['metadata'],
    output:
        multiqc=kraken_reads+"kraken_reads_per_species_mqc.txt",#final result as input for MQC
        xls=kraken_reads+"kraken_reads_per_species.xls",#final result as XLS
        csv=kraken_reads+"kraken_reads_per_species.csv",#final result as XLS
    log:
        kraken_log+"kraken_report_sepcies.log"
    benchmark:
        kraken_bench+"kraken_report_sepcies.txt"
    params:
        cut="S"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/kraken_report.R"

"""
create kraken species contigs  report
"""
rule kraken_report_contigs_species:
    input:
        localres=expand(kraken_contigs+"{sample}/{sample}.report.txt",sample=SAMPLES),
        metadata=config['metadata'],
    output:
        multiqc=kraken_contigs+"kraken_contigs_per_species_mqc.txt",#final result as input for MQC
        xls=kraken_contigs+"kraken_contigs_per_species.xls",#final result as XLS
        csv=kraken_contigs+"kraken_contigs_per_species.csv",#final result as csv
    log:
        kraken_log+"kraken_report_contigs_sepcies.log"
    benchmark:
        kraken_bench+"kraken_report_contigs_sepcies.txt"
    params:
        cut="S"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/kraken_report.R"




"""
create kraken genera  report for reads
"""
rule kraken_report_reads_genera:
    input:
        localres=expand(kraken_reads+"{fastq}/{fastq}.report.txt",fastq=FASTQ),
        metadata=config['metadata'],
    output:
        multiqc=kraken_reads+"kraken_reads_per_genera_mqc.txt",#final result as input for MQC
        xls=kraken_reads+"kraken_reads_per_genera.xls",#final result as XLS
        csv=kraken_reads+"kraken_reads_per_genera.csv",#final result as csv
    log:
        kraken_log+"mqc_report_reads_genera.log"
    benchmark:
        kraken_bench+"mqc_report_reads_genera.txt"
    params:
        cut="G"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/kraken_report.R"
