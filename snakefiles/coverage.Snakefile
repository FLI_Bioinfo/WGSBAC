


"""
Calculate coverage for each sample
"""
rule coverage:
    input:#input is unzipped fastq-files AND reference genome assembly
      fw=unzip_res+"{fastq}_R1.fastq",
      rv=unzip_res+"{fastq}_R2.fastq",
      ref=config["refstrain"],
     # scripts=scripts,
    output:
      out=coverage_res+"{fastq}.csv"#coverage of each sample
    benchmark:
       coverage_bench+"{fastq}.txt"
    log:
       coverage_log+"{fastq}.log"
    conda:
      envs + "coverage.yaml"
    shell:#adapted from https://github.com/raymondkiu/fastq-info/blob/master/fastq_info_3.sh
       "sh {scripts}fastq_info_ma.sh  {input.fw} {input.rv} {input.ref} > {output.out} 2>{log}"

"""
Collects coverage result from ech sample and combines in one file
"""
rule collect_coverage:
    input:#coverage from each sample
        localres=expand(coverage_res+"{fastq}.csv", fastq=FASTQ),
        metadata=config['metadata'],
    output:#results in csv and xls
        allcsv=coverage_res+"allcoverage.csv",
        allxls=coverage_res+"allcoverage.xlsx",
        multiqc=coverage_res+"Coverage_fastq_info_mqc.txt",
    log:
       coverage_log+"allcoverage.log"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineCoverage.R"#converts to xls
