"""----------------------------
{joerg.linde;mostafa.abdel-glil}@fli.de, date: December, 05, 2018, Friedrich-Loeffler-Institut (https://www.fli.de/)
-------------------------------
Use this to define resectories variables
important not to forget the slash at the end of the resectory name
"""

"""
data folder for databases and ohter data
"""
"""
results folders
"""

#general results
#input="input/"#zipped  or unzipped fastq files
#results="results/"#general results
#standard assembly and QC
unzip_res=results+"fastq/"#unzipped fastq files
fastqc_res=results+"fastqc/"#fastqc
fastp_res=results+"fastp/"#fastqc
rawassembly_res=results+"rawassembly_res/"#folder holding raw assembly files (f.e from spades or shovill)
filtering_res=results+"filteredAssembly/"#filtered contigs
renaming_res=results+"finalAssembly/"#renamed contigs
quast_res=results+"quast/"#quast
kraken_res=results+"kraken/"#kraken
kraken_reads=kraken_res+"reads/"#kraken read based
kraken_contigs=kraken_res+"contigs/"#kraken contig based
sourmash_res=results+"sourmash/"#sourmash
krona_res=results+"krona/"#krona resuls
coverage_res=results+"coverage/"#coverage
multiqc_res=results+"multiqc"#multiqc general do NOT us tailing "/" for folder wihch are used with directory()
multiqc_fastqc=results+"multiqc_fastqc"#multiqc for fastqc  do NOT us tailing "/" for folder wihch are used with directory()
assemblies_renamed=results+"renamed_assemblies"#folder with copies of assemblies using common name instead of ID. Used for Seqsphere
confindr_res=results+"confindr/"#final results confindr


##trees
trees=results+"trees/"#folder holding all pyhlogenetic trees
itol=trees+"itolmeta"#folder holding metadata as input for itol
itolclustsnps=trees+"itolclustsnps"#folder holding clustering data as input for itol
itolclustmlva=trees+"itolclustmlva"#folder holding clustering data as input for itol
itolmlst=trees+"itolmlst"#folder holding mlst data  as input for itol
clustering=trees+"clustering/"#folder holding clustering results

##annotation
prokka_res=results+"prokka/" #prokka
bakta_res=results+"bakta/"
##typing
cansnper_res=results+"cansnper/"#cansnper
cansnper2_res=results+"cansnper2/"#cansnper
parsnp_res=results+"parsnp/"#parsnp
mlst_res=results+"mlst/"#mlst
panaroo_res=results+"panaroo/"#results from snippy
snippy_folder=results+"snippy/"#results from snippy
snippy_core=snippy_folder+"core/"#results from snippy core
raxml_res_snippy=results+"raxml_snippy/"#results for raxml
raxml_res_panaroo=results+"raxml_panaroo/"#results for raxml
mlvafinder_res=results+"MLVA_finder/"#results for MLVA_finder


##

##serotyping
sistr_res=results+"sistr/"#sistr
seqSero_res=results+"seqSero/"#seqSero
seqSero_reads=seqSero_res+"reads/"#seqSero with reads
seqSero_contigs=seqSero_res+"contigs/"#seqSero with contigs
ecolitype_res=results+"ecolityping/"#Ecoy serotyping and clermont typing resuls


#AMR
amr_res=results+"amr/"#abricate VS CARDm Resfinder,NCBI and AMRfinderplus
#virulence
virulence_res=results+"virulence/"#abricate VS VFDB
#plasmids
plasmids_res=results+"plasmids/"#abricate VS plasmidfinder, platon
platon_res=plasmids_res+"platon/"#platon
plasmidfinder_res=plasmids_res+"plasmidfinder/"#platon

#clostridia toxin typing
clostox_res=results+"clostridia_toxin_typing/"#clostridia toxin typing
#Arcobacter DB
arco_res=results+"arcobacterDB/"


"""
benchmarking folders
"""
fastqc_bench=benchmark+"fastqc/"#fastqc
fastp_bench=benchmark+"fastp/"#fastqc
unzip_bench=benchmark+"unzip/"#unzip
coverage_bench=benchmark+"coverage/"#unzip
spades_bench=benchmark+"spades/"#spades
filtering_bench=benchmark+"finalAssembly/"#filtered and rename contigs
quast_bench=benchmark+"quast/"#quast
kraken_bench=benchmark+"kraken/"#kraken
krona_bench=benchmark+"krona/"#krona resuls
multiqc_bench=benchmark+"multicq/"#multiqc
itol_bench=benchmark+"itol/"#itol
snippy_bench=benchmark+"snippy/"
shovill_bench=benchmark+"shovill/"
sourmash_bench=benchmark+"sourmash/"
confindr_bench=benchmark+"confindr/"

##annotation
prokka_bench=benchmark+"prokka/" #prokka
bakta_bench=benchmark+"bakta/" #

##typing
cansnper_bench=benchmark+"cansnper/"#cansnper
cansnper2_bench=benchmark+"cansnper2/"#cansnper
parsnp_bench=benchmark+"parsnp/"#parsnp
ksnp_bench=benchmark+"ksnp/"#ksnp
mlst_bench=benchmark+"mlst/"#mlst
panaroo_bench=benchmark+"panaroo/"#panaroo
mlvafinder_bench=benchmark+"MLVA_MIStress/"#results for MLVA_finder
ecolitype_bench=benchmark+"ecolityping/"#Ecoy serotyping and clermont typing resuls



##sistr
sistr_bench=benchmark+"sistr/"#sistr
seqSero_bench=benchmark+"seqSero/"#seqSero
#AMR
amr_bench=benchmark+"amr/"#abricate VS CARDm Resfinder,NCBI and AMRfinderplus
#virulence
virulence_bench=benchmark+"virulence/"#abricate VS VFDB
#plasmids
plasmids_bench=benchmark+"plasmids/"#abricate VS plasmidfinder, platon
#clostridia toxin typing
clostox_bench=benchmark+"clostridia_toxin_typing/"#clostridia toxin typing
#Arcobacter DB
arco_bench=benchmark+"arcobacterDB/"



"""
loggin folders
"""

fastqc_log=log+"fastqc/"#fastqc
fastp_log=log+"fastp/"#fastqc
unzip_log=log+"unzip/"#unzip
coverage_log=log+"coverage/"#unzip
spades_log=log+"spades/"#spades
filtering_log=log+"finalAssembly/"#filtered and rename contigs
quast_log=log+"quast/"#quast
kraken_log=log+"kraken/"#kraken
krona_log=log+"krona/"#krona resuls
multiqc_log=log+"multiqc/"
itol_log=log+"itol/"#itol
snippy_log=log+"snippy/"#snippy
shovill_log=log+"shovill/"
sourmash_log=log+"sourmash/"
confindr_log=log+"confindr/"
ecolitype_log=log+"ecolityping/"#Ecoy serotyping and clermont typing resuls

#annotation
prokka_log=log+"prokka/" #prokka
bakta_log=log+"bakta/" #

##typing
parsnp_log=log+"parsnp/"#parsnp
cansnper_log=log+"cansnper/"#cansnper
cansnper2_log=log+"cansnper2/"#cansnper
ksnp_log=log+"ksnp/"#ksnp
mlst_log=log+"mlst/"#mlst
panaroo_log=log+"panaroo/"#panaroo
mlvafinder_log=log+"MLVA_finder/"#results for MLVA_finder


##serotyping
sistr_log=log+"sistr/"#sistr
seqSero_log=log+"seqSero/"#seqSero

#AMR
amr_log=log+"amr/"#abricate VS CARDm Resfinder,NCBI and AMRfinderplus
#virulence
virulence_log=log+"virulence/"#abricate VS VFDB
#plasmids
plasmids_log=log+"plasmids/"#abricate VS plasmidfinder, platon
#clostridia toxin typing
clostox_log=log+"clostridia_toxin_typing/"#clostridia toxin typing
#Arcobacter DB
arco_log=log+"arcobacterDB/"





"""
tmp folders
"""
#tmp="tmp/"#general
assemblies_tmp=tmp+"assemblies/"#folder holding all assemblies as input for downstream tools (e.g. parsnp, ksnp,...)
fastqc_tmp=tmp+"fastqc/"#folder holding all fastqc results. needed for multiqc
quast_tmp=tmp+"quast/"#folder holding all quast results. needed for multiqc
abricate=tmp+"abricate/"#folder holding all assemblies as input for downstream tools (e.g. parsnp, ksnp,...)
panaroo_tmp=tmp+"panaroo/"#folder holding tmp panaroo results.
mlvafinder_tmp=tmp+"mlvafinder/"#folder holding tmp mistree results.
fastq_tmp=tmp+"fastq/"#folder holding all fastq files as input for confindr
cansnper2_tmp=tmp+"cansnper2/"#cansnper2 tmp files
