"""
Quality controll FASTQC
"""
rule fastp:
    input:#receive all forward and reverse fastq files
        fw= unzip_res+"{fastq}_R1.fastq",
        rv= unzip_res+"{fastq}_R2.fastq",
    output:
        json=fastp_res+"{fastq}"+".json",#creates output directory
        html=fastp_res+"{fastq}"+".html",#creates output directory
    threads: 4
    params:
        additional=config["fastp_params"]
    benchmark:
        fastp_bench+"{fastq}.txt"
    log:
        fastp_log+"{fastq}.log"
    conda:
      envs + "fastp.yaml"
    shell:#create output directories before running fastqc for each forward and reverse reads seperately
        "fastp -i {input.fw} -I {input.rv} --thread {threads} {params.additional} --json {output.json} --html {output.html} &> {log}"


rule collect_fastp:
    input:#input is
        fastp=expand(fastp_res+"{fastq}"+".json", fastq=FASTQ),
    output:#outpus as csv and xls
        q30_csv=fastp_res+"allq30.csv",
        q30_xls=fastp_res+"allq30.xlsx",
        multiqc=fastp_res+"allq30_mqc.txt",
    log:
        fastp_log+"allfastp.log"
    benchmark:
        fastp_bench+"allfastp.log"
    conda:
        envs + "fastp.yaml"
    script:
        "../scripts/collect_fastp.R"#converts to xls
