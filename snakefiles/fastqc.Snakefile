"""
Quality controll FASTQC
"""
rule fastqc:
    input:#receive all forward and reverse fastq files
        fw= unzip_res+"{fastq}_R1.fastq",
        rv= unzip_res+"{fastq}_R2.fastq",
    output:
        outdir1=directory(fastqc_res+"{fastq}"),#creates output directory
    threads: 4
    params:
        additional=config["fastqc_params"]
    benchmark:
        fastqc_bench+"{fastq}.txt"
    log:
        fastqc_log+"{fastq}.log"
    conda:
      envs + "fastqc.yaml"
    shell:#create output directories before running fastqc for each forward and reverse reads seperately
        "mkdir {output.outdir1} && fastqc -t {threads} -o {output.outdir1} {params.additional} {input.fw} {input.rv}  &> {log}"
