
"""
converts metadata to folder of filses as input for itol
"""
rule meta_to_itol:
    input:#input is
        metadata=config['metadata'],
    output:#outpus as csv and xls
        folder=directory(itol),
    log:
        itol_log+"meta_to_itol.log"
    benchmark:
        itol_bench+"meta_to_itol.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/table2itol.R"#creates itol input

"""
converts clustering results  to folder of filses as input for itol
"""
rule snp_cluster_to_itol:
    input:#input is
        metadata=tmp+"clustall_snpcore.csv"
    output:#outpus as csv and xls
        folder=directory(itolclustsnps),
    log:
        itol_log+"cluster_to_itol.log"
    benchmark:
        itol_bench+"cluster_to_itol.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/table2itol.R"#creates itol input
"""
converts clustering results  to folder of filses as input for itol
"""
rule mlva_cluster_to_itol:
    input:#input is
        metadata=clustering+"hclust_mlva.csv",
    output:#outpus as csv and xls
        folder=directory(itolclustmlva),
    log:
        itol_log+"cluster_to_itol_mlva.log"
    benchmark:
        itol_bench+"cluster_to_itol_mlva.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/table2itol.R"#creates itol input


"""
converts mlst results  to folder of filses as input for itol
"""
rule mlst_to_itol:
    input:#input is
        metadata=mlst_res+"allMLST.csv",
    output:#outpus as csv and xls
        folder=directory(itolmlst),
    log:
        itol_log+"mlst_to_itol.log"
    benchmark:
        itol_bench+"mlst_to_itol.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/table2itol.R"#creates itol input
