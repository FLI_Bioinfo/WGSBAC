"""----------------------------
{joerg.linde;mostafa.abdel-glil}@fli.de,  Friedrich-Loeffler-Institut (https://www.fli.de/)
-------------------------------
This workflow aims at bacterial  genotyping and characterisation based on paired-end NGS data
"""
#general folders taken from config files
input=config["input"]
results=config["results"]
log=config["log"]
benchmark=config["benchmark"]
tmp=config["tmp"]
snakefiles= config["snakefiles"]#"snakefiles/"
envs=config["envs"]
bin=config["bin"]
scripts=config["scripts"]
#SAMPLES=config["samples"]
#FASTQ= config["fastq"]


#WGSbac specific folders
include: snakefiles + "defineFolders.Snakefile"
#general variables
FASTQGZ, = glob_wildcards( input + "{fastqz}_R1.fastq.gz")#zipped fastq files
FASTQUZ, =glob_wildcards( unzip_res + "{fastq}_R1.fastq")#unzipped fastq files
GENOMES, = glob_wildcards( renaming_res + "{genomes}.fasta")#assemblies
FASTQGZ=set(FASTQGZ)
FASTQUZ=set(FASTQUZ)
FASTQ=  FASTQGZ| FASTQUZ#all fastq files
GENOMES=set(GENOMES)
SAMPLES=FASTQ| GENOMES#all samples

#include statements for tasks
#obligatory tasks
include: snakefiles + "fastqc.Snakefile"
include: snakefiles + "fastp.Snakefile"
include: snakefiles + "unzip.Snakefile"
include: snakefiles + "coverage.Snakefile"
include: snakefiles + "shovill.Snakefile"
include: snakefiles + "rename_contigs.Snakefile"
include: snakefiles + "quast.Snakefile"
include: snakefiles + "contamination.Snakefile"
include: snakefiles + "assemblyfolder.Snakefile"
include: snakefiles + "multiqc.Snakefile"
include: snakefiles + "sourmash.Snakefile"
include: snakefiles + "combineQC.Snakefile"

#all possible results files. Dependent on the user selection, WGSBAC computes a subsample of these results
coverage=coverage_res+"allcoverage.csv",#calculates coverage for all samples in one table
#renaming=expand(renaming_res+"{sample}.fasta", sample=sample),#creates filtered assemblies for all samples
quast=quast_res+"allquast.csv",
q30=fastp_res+"allq30.csv",
prokka=expand(prokka_res + "{sample}/{sample}.gff", sample=SAMPLES),
bakta=expand(bakta_res + "{sample}/", sample=SAMPLES),
sistr=sistr_res+"allSISTRres.csv",
kraken_reads_species=kraken_reads+"kraken_reads_per_species.xls",#kraken reads on species level result as XLS
kraken_contigs_species=kraken_contigs+"kraken_contigs_per_species.xls",#kraken contigs on species level result as XLS
kraken_reads_genera=kraken_reads+"kraken_reads_per_genera.xls",#kraken reads on genus level result as XLS
parsnp=trees+"parSNPtree.nw",#parSNP
mlst=mlst_res+"allMLST.csv", #MLST typing
cansnper=cansnper_res+"allcanSNPer.csv",
cansnper2=cansnper2_res+"allcanSNPer2.csv",
multiqc_fastq=multiqc_fastqc,#multiqc report from FastQC
multiqc_rep=multiqc_res,#multiqc report fro remaining
assemblies_renamed,#assemblies with common.name as filename (input for other tools)
itol=itol,#config files for itol based on meta dats
raxml_snippy=trees+"snippy.nw",#tree from snippy
snippy_dist=snippy_core+"snpdist.xls"#pairwise snp distances
snpclust=clustering+"hclust_snps.xls"#hier clusterig and hierBAPS of snippy results
itolclustsnps=itolclustsnps#config files for itol based on SNP clustering
itolclustmlva=itolclustmlva#config files for itol based on MLVA clustering
itolmlst=itolmlst#config files for itol based on mlst
seqSero=seqSero_res+"allseqSero.csv"
abricate_ncbi=amr_res+"all_ncbi.csv"
abricate_card=amr_res+"all_card.csv"
abricate_vfdb=virulence_res+"all_vfdb.csv"
abricate_plasmidfinder=plasmidfinder_res+"all_plasmidfinder.csv"
abricate_resfinder=amr_res+"all_resfinder.csv"
panaroo=panaroo_res,
raxml_panaroo=trees+"panaroo_raxml.nw",
seqsero=seqSero_res+"allseqSero.csv",
mlva=trees+"mlva.nw",
amrfinder=amr_res+"allAmrfinder.xlsx",
abricate_spi=virulence_res+"all_spi.xls",#output XLS format
abricate_clostox=clostox_res+"all_clostox.xls",#output XLS format
abricate_arcodbvir=arco_res+"all_arcodbvir.xls"
abricate_arcodbamr=arco_res+"all_arcodbamr.xls",#output XLS format
platon=platon_res+"all_platon.xls",#output XLS format
qc=results+"combinedQC.csv",#output XLS format
qc=results+"combinedQC.csv",#output XLS format
confindr=results+"allConfindR.xlsx",
ecoh=ecolitype_res+"all_ecoh.xls",#output XLS format
ecoliclermont=ecolitype_res+"ezclermont_summary.tsv",#Ecoli Clermont phylogrup
