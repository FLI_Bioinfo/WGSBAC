

"""
MLST alleles detection from WGS data using mlst tool (classical, 6-10 alleles)

"""
#updates schemes
rule update_schemes:
    output:#directory holding all mlst results for each sample
        schemefile=config["mlst_file"]#path to list of schmes
    log:
        mlst_log+"update_schemes.log"
    benchmark:
        mlst_bench+"update_schemes.txt"
    conda:
        envs+ "mlst.yaml"
    message:
        "Download mlst schemes\n"
    shell:
        #"touch {output.schemefile}"
        "var=\"$(which mlst)\" &&\
        mlstfolder=\"${{var%/bin/mlst}}\" &&\
        mlstscripts=\"$mlstfolder/scripts/\" &&\
        mlstdb=\"$mlstfolder/db/\" &&\
        perl $mlstscripts/mlst-download_pub_mlst -d $mlstdb &&\
        perl $mlstscripts/mlst-make_blast_db &&\
        mlst --longlist 1>{output.schemefile} 2>{log}"


#performs mlst fot each assembly
rule mlst:
    input:#input is final assemblies after spades and filtering with coverage and length
        fasta=renaming_res+"{sample}.fasta",
        schemefile=config["mlst_file"],#path to list of schmes
    output:#directory holding all mlst results for each sample
        mlst=mlst_res+"{sample}.mlst.txt",
    log:
        mlst_log+"{sample}.log"
    benchmark:
        mlst_bench+"{sample}.txt"
    conda:
        envs+ "mlst.yaml"
    params:
        scheme=config["mlst_scheme"],#selected scheme from user
        additional=config["mlst_params"]
    shell:
        "mlst  {input.fasta} --scheme {params.scheme}  --nopath {params.additional}  1> {output.mlst} 2>{log} || touch {output.mlst}"


"""
Combines single MLST results and converst into XLS and MQC report
"""
rule collect_mlst:
    input:#input is
        localres= expand(mlst_res+"{sample}.mlst.txt", sample=SAMPLES),
        metadata=config['metadata'],
    output:#outpus as csv and xls
        allcsv=mlst_res+"allMLST.csv",
        allxls=mlst_res+"allMLST.xlsx",
        multiqc=mlst_res+"Classical_MLST_mqc.txt",
    log:
        mlst_log+"collect_mlst.log"
    benchmark:
        mlst_bench+"collect_mlsttxt"
    conda:
        envs+ "rxls.yaml"
    script:
        "../scripts/combineMLST.R"#converts to xls
