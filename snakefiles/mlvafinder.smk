"""
Runs MLVAFinder https://github.com/i2bc/MLVA_finder
"""
rule MLVAfinder:
    input:
        assemblies=assemblies_tmp,
    output:
        dir=temp(directory(mlvafinder_tmp)),
    benchmark:
        mlvafinder_bench+"mlvafinder.txt"
    params:
        primers=config["mlvafinder_primers"],
        additional=config["mlvafinder_params"]
    conda:
        envs+ "mlvafinder.yaml"
    log:
        mlvafinder_log+"mlvafinder.log"
    shell:
      "python3 {scripts}MLVA_finder.py -i {input.assemblies} -o {output.dir} -p {params.primers} {params.additional} > {log} 2>&1"


rule cpmlvafinder:#copies relevant results from tmp dir
    input:
        mlvafinder_tmp=mlvafinder_tmp
    output:
        mismatches=mlvafinder_res+"assemblies_mismatchs.txt",
        output=mlvafinder_res+"assemblies_output.csv",
        pcr=mlvafinder_res+"predicted_PCR_size_table_assemblies.csv",
    log:
        mlvafinder_log+"cpmlvafinder.log"
    shell:
        "cp {input.mlvafinder_tmp}/assemblies_mismatchs.txt  {output.mismatches}  2>{log} &&\
        cp {input.mlvafinder_tmp}/assemblies_output.csv  {output.output}  2>{log} &&\
        cp {input.mlvafinder_tmp}/predicted_PCR_size_table_assemblies.csv  {output.pcr}  2>{log}"

"""
collects results form mlvafinder
"""
rule collect_mlvafinder:
    input:#input is
        mlvafinder=mlvafinder_tmp,
        metadata=config['metadata'],
    params:
        maxvntrs=config['hclust_maxvntrs'],#200,100,50,20,10,5,1
        minmember=config['hclust_minmembermlv'],#2
    output:#outpus as csv and xls
        allcsv=mlvafinder_res+"allMLVA.csv",
        allxls=mlvafinder_res+"allMLVA.xlsx",
        multiqc=mlvafinder_res+"MLVA_mqc.txt",
        dists=mlvafinder_res+"VNTR_dist.xls",
        newick=trees+"mlva.nw",
        hclust=clustering+"hclust_mlva.xls",
	    hclustcsv=clustering+"hclust_mlva.csv",
        cluststats=clustering+"cluststats_mlva.xls"
    log:
        mlvafinder_log+"collectmlvafinder.log"
    benchmark:
        mlvafinder_bench+"collectmlvafinder.txt"
    conda:
        envs + "rclust.yaml"
    script:
        "../scripts/combinemlvafinder.R"#converts to xls
