"""
MultiQC for FastQC
"""


"""
Helper: prepares a folders holding all results from fastqc
needed to link all files using expand otherwise snakemake will fail
"""
rule prepare_folder:
    input:
        # Input are all (own,given) assemblies
        fastqc=expand(fastqc_res+"{fastq}", fastq=FASTQ),#fastqc
        quast=expand(quast_res+"{sample}", sample=SAMPLES),#quast
    output:
        #  directories for all results
        tmp_fastqc=temp(directory(fastqc_tmp)),
        tmp_quast=temp(directory(quast_tmp))
    benchmark:
        benchmark+"prepare_folder_multiqc.txt"
    log:
        log+"prepare_folder_multiqc.log"
    run:
        if not os.path.exists(output.tmp_fastqc):
            os.makedirs(output.tmp_fastqc, exist_ok=True)
        for fastqc in input.fastqc:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(fastqc)
            head, tail = os.path.split(fastqc) #warning; does not work if contig ends with / or \
            outfile = str(output.tmp_fastqc)+"/"+tail
            shell("if [ ! -f {outfile} ]; then ln -s {abspath} {outfile} >{log}; fi")
        if not os.path.exists(output.tmp_quast):
            os.makedirs(output.tmp_quast, exist_ok=True)
        for quast in input.quast:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(quast)
            head, tail = os.path.split(quast) #warning; does not work if contig ends with / or \
            outfile = str(output.tmp_quast)+"/"+tail
            shell("if [ ! -f {outfile} ]; then ln -s {abspath} {outfile} >{log}; fi")

"""
MultiQC for FastQC
"""
rule multiqc_fastqc:
    input:
        fastqc=fastqc_tmp,#takes tmp folder as input
    output:
        results=directory(multiqc_fastqc)#all
    threads:
        8
    conda:
        envs+ "multiqc.yaml"
    log:
        multiqc_log+"multiqc_fastqc.log"
    benchmark:
        multiqc_bench+"multiqc_fastqc.txt"
    shell:
        "multiqc --filename fastqc_assembly_report --outdir {output.results} {input.fastqc}"

"""
MultiQC for non fastqc
"""
rule multiqc:
    input:
        quast=quast_tmp,#takes tmp folder as input
        q30=fastp_res+"allq30_mqc.txt",#q30 results calculated by fastp
        kraken_species=kraken_reads+"kraken_reads_per_species_mqc.txt",#kraken results for reads and species
        kraken_genus=kraken_reads+"kraken_reads_per_genera_mqc.txt",#kraken results for reads and genus
        kraken_species_assemblies=kraken_contigs+"kraken_contigs_per_species_mqc.txt",#kraken results for contigs and species
        sourmash=sourmash_res+"Sourmash_mqc.txt",#mash uses kmers to iedntify genus, species and strain
        coverage=coverage_res+"Coverage_fastq_info_mqc.txt",#coverage
        cansnper=cansnper_res+"CanSNPer_Subclades_mqc.txt" if re.search("cansnper",config["report_params"]) else snakefiles, #uses cansper results only if user added parameter, if not uses snakefiles as dummy
        cansnper2=cansnper2_res+"CanSNPer2_Subclades_mqc2.txt" if re.search("canSNPer2",config["report_params"]) else snakefiles, #uses cansper results only if user added parameter, if not uses snakefiles as dummy
        abricate_plasmidfinder=plasmidfinder_res+"Abricate_Plasmidfinder_mqc.txt" if re.search("plasmids",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        abricate_vfdb=virulence_res+"Abricate_vfdb_mqc.txt" if re.search("virulence",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        abricate_resfinder=amr_res+"Abricate_Resfinder_mqc.txt" if re.search("amr",config["report_params"]) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        abricate_card=amr_res+"abricate_card_mqc.txt"if re.search("amr",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        abricate_ncbi=amr_res+"Abricate_ncbi_mqc.txt"if re.search("amr",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        amrfinder=amr_res+"AMRfinder_mqc.txt" if re.search("amr",config["report_params"]) else snakefiles, #uses amrdinder results only if user added parameter, if not uses snakefiles as dummy
        spi=virulence_res+"abricate_spi_mqc.txt" if re.search("spi",config["report_params"]) else snakefiles, #uses amrdinder results only if user added parameter, if not uses snakefiles as dummy
        arcoamr=arco_res+"abricate_arcodbamr_mqc.txt" if re.search("arco",config["report_params"]) else snakefiles, #uses amrdinder results only if user added parameter, if not uses snakefiles as dummy
        arcovir=arco_res+"abricate_arcodbvir_mqc.txt" if re.search("arco",config["report_params"]) else snakefiles, #uses amrdinder results only if user added parameter, if not uses snakefiles as dummy
        clostox=clostox_res+"abricate_clostox_mqc.txt" if re.search("clostox",config["report_params"]) else snakefiles, #uses amrdinder results only if user added parameter, if not uses snakefiles as dummy
        mlst= mlst_res+"Classical_MLST_mqc.txt" if re.search("mlst",config["report_params"])   else snakefiles, #uses mlst results only if user added parameter, if not uses snakefiles as dummy
        sistr=sistr_res+"SISTR_results_mqc.txt" if re.search("sistr",config["report_params"]) else snakefiles, #uses sistr results only if user added parameter, if not uses snakefiles as dummy
        seqsero=seqSero_res+"SeqSero_results_mqc.txt" if re.search("seqsero",config["report_params"]) else snakefiles, #uses seqsero results only if user added parameter, if not uses snakefiles as dummy
        snippy=snippy_core+"Snipdist_mqc.txt"if re.search("snippy",config["report_params"]) else snakefiles, #uses snippy results only if user added parameter, if not uses snakefiles as dummy
        mlva=mlvafinder_res+"MLVA_mqc.txt" if re.search("mlva",config["report_params"]) else snakefiles, #uses mlva results only if user added parameter, if not uses snakefiles as dummy
        platon=platon_res+"Platon_mqc.txt" if re.search("plasmids",config["report_params"]) else snakefiles, #uses mlva results only if user added parameter, if not uses snakefiles as dummy
        confindr=results+"ConFindR_mqc.txt" if re.search("confindr",config["report_params"]) else snakefiles, #uses mlva results only if user added parameter, if not uses snakefiles as dummy
        ecoliserotype=ecolitype_res+"Abricate_ecoh_mqc.txt" if re.search("ecolitype",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        ecovir=ecolitype_res+"Abricate_ecoli_vf_mqc.txt" if re.search("ecolitype",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy
        ecoliclermont=ecolitype_res+"Clermont_mqc.txt" if re.search("ecolitype",config["report_params"] ) else snakefiles, #uses abricate results only if user added parameter, if not uses snakefiles as dummy

        #roary= roary_res + "gene_presence_absence.csv"if re.search(config["report_params"] , "roary") else snakefiles, #uses roary results only if user added parameter, if not uses snakefiles as dummy
    output:
        results=directory(multiqc_res)#all
    conda:
        envs+ "multiqc.yaml"
    log:
        multiqc_log+"multiqc.log"
    benchmark:
        multiqc_bench+"multiqc.txt"
    shell:
        "multiqc --filename multiqc_report --outdir {output.results} {input.quast} {input.q30}  {input.coverage} {input.kraken_genus} {input.kraken_species}  {input.kraken_species_assemblies} {input.sourmash} {input.confindr} {input.cansnper} {input.cansnper2} {input.mlst} {input.sistr} {input.ecoliclermont} {input.ecoliserotype} {input.ecovir}  {input.abricate_plasmidfinder} {input.platon} {input.amrfinder}  {input.abricate_resfinder} {input.abricate_card} {input.abricate_ncbi} {input.arcoamr} {input.abricate_vfdb} {input.arcovir} {input.spi} {input.clostox}  {input.seqsero} {input.snippy} {input.mlva}"
