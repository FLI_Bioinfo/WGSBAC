
"""
Roary for pan-genome analysis

"""
rule Panaroo: #run panaroo
    input:
        bakta_gff= expand(bakta_res + "{sample}/{sample}.gff3", sample=SAMPLES),
    output:
        outdir=directory(panaroo_res),
    threads: 32
    log:
        panaroo_log + "panaroo.log"
    conda:
        envs +"panaroo.yaml"
    benchmark:
        panaroo_bench+"panaroo.txt"
    params:
        additional=config["panaroo_params"],
    shell:
        "panaroo -i {input.bakta_gff}  -o {output.outdir} {params.additional} -t {threads}  | tee {log}"
