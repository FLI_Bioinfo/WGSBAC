


"""
Runs parSNP on assembly folder.
"""
rule parsnp:
    input:
        assemblies=assemblies_tmp,
        ref=config["refstrain"]
    output:#output is folder parsnp
        directory(parsnp_res),
    threads:
        8
    params:
        additional=config["parsnp_params"]
    benchmark:
        parsnp_bench+"parsnp.txt"
    conda:
        envs+ "parsnp.yaml"
    log:
        parsnp_log+"parsnp.log"
    shell:
        "if [ -f {input.assemblies}/.snakemake_timestamp ]; then rm {input.assemblies}/.snakemake_timestamp; fi && parsnp -c -r {input.ref}  -d {input.assemblies} -p {threads}  -v -o {output} {params.additional} > {log} 2>&1"

"""
helper
copies parsnp.tree to a tmp directory as drawParSNPtree needs a file as input not directoy
"""
rule cpparsnp:
    input:
        parsnp=parsnp_res
    output:
        res=temp(tmp+"parsnp.tree")
    shell:
        "cp {input.parsnp}/parsnp.tree  {output.res}"


"""
Draws tree from parsnp results including subtype name from canSNPer
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed samples
"""
rule drawParSNPtree:
    input:
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        #Always us NEwick as variable name
        newick=tmp+"parsnp.tree",#tree from parsnp in newick fromat
    output:
        #Always use pdf as variable name
#        pdf=trees+"parSNPtree.pdf",
        newick=trees+"parSNPtree.nw"#tree in newick format with exchanded IDs into common names
    params:
        #alsways use params as variable name
        title="Phylogenetic tree from parSNP"
    conda:
        envs+ "rcran.yaml"
    log:
        parsnp_log+"drawparSNP.log"
    benchmark:
        parsnp_bench+"drawparSNP.txt"
    script:
        "../scripts/plottree.R"
