

#downlads plasmidfinder if older than 30 days
rule plasmidfinderdownload:
    output:#directory holding all mlst results for each sample
        db=config["abricateplasmidsfinder"],
    log:
        amr_log+"downloadabricateplasmidfinder.log"
    benchmark:
        amr_bench+"downloadabricateplasmidfinder.txt"
    conda:
        envs+ "abricate.yaml"
    shell:
        " abricate-get_db --db plasmidfinder --force  2>{log}&&\
	touch {output.db}  && chmod 777 {output.db}  2>>{log}"



rule abricate_plasmidfinder:
    input:
        fasta=renaming_res+"{sample}.fasta",
	    db=config["abricateplasmidsfinder"],
    output:
        plasmidfinder=plasmidfinder_res+"{sample}_plasmidfinder.csv",
    log:
        plasmidfinder=plasmids_log+"{sample}_plasmidfinder.log",
    threads: 1
    benchmark:
        plasmids_bench+"abricate.plasmidfinder.{sample}.txt"
    params:
        additional=config["plasmids_abricate_params"]
    conda:
      envs + "abricate.yaml"
    shell:
      "(abricate --threads {threads} {input.fasta}  --db plasmidfinder  {params.additional} >{output.plasmidfinder} 2>>{log.plasmidfinder}) || touch {output.plasmidfinder}"



"""
Combines/summarizes Abricate results in one single CSV
"""
rule summary_plasmidfinder:
    input:
        plasmidfinder=expand(plasmidfinder_res+"{sample}_plasmidfinder.csv", sample=SAMPLES),
    output:
        plasmidfinder_csv=temp(abricate+"plasmidfinder.csv"),
    log:
        plasmidfinder=plasmids_log+"combine_abricate_plasmidfinder.log",
    benchmark:
        plasmids_bench+"combine_abricate.txt"
    conda:
      envs + "abricate.yaml"
    shell:
      "abricate  --summary  {input.plasmidfinder}   >{output.plasmidfinder_csv} 2>>{log.plasmidfinder}"




"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
"""
rule collect_abbricate_plasmidfinder:
    input:#input is
        abricateres=abricate+"plasmidfinder.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=plasmidfinder_res+"all_plasmidfinder.csv",#output csv format
        abricate_xls=plasmidfinder_res+"all_plasmidfinder.xls",#output XLS format
        abricate_mqc=plasmidfinder_res+"Abricate_Plasmidfinder_mqc.txt",#output MQC format
    params:
        db="plasmidfinder"
    log:
        plasmids_log+"collect_plasmidfinder.log"
    benchmark:
        plasmids_bench+"collect_plasmidfindertxt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls

"""
Runs platon https://github.com/oschwengers/platon
"""

rule platon:
    input:
        fasta=renaming_res+"{sample}.fasta",
    output:
        folder=directory(platon_res+"{sample}"),
#        file=v+"{sample}_platon.csv",
    log:
        plasmids_log+"{sample}_platon.log",
    threads: 4
    benchmark:
        plasmids_bench+"platon.{sample}.txt"
    params:
        additional=config["platon_params"],
        db=config["platon_db"]
    conda:
      envs + "platon.yaml"
    shell:
      "(platon --threads {threads} --verbose  --db {params.db}  {params.additional}  {input.fasta} --output {output.folder}  >{log} 2>&1) || mkdir {output.folder}"

"""
Cobines platon results in XLS and MQC report
"""

rule collect_platon:
    input:#input is
        platonres=expand(platon_res+"{sample}", sample=SAMPLES),
        metadata=config['metadata'],
    output:#outpus as csv and xls
        platon_csv=platon_res+"all_platon.csv",#output csv format
        platon_xls=platon_res+"all_platon.xls",#output XLS format
        platon_mqc=platon_res+"Platon_mqc.txt",#output MQC format
    log:
        plasmids_log+"collect_platon.log"
    benchmark:
        plasmids_bench+"collect_platon.txt"
    conda:
      envs + "rxls.yaml"
    script:
        "../scripts/combinePlaton.R"#converts to xls
