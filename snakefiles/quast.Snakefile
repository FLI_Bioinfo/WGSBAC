
"""
Quast for statistics about all final assemblies
"""
rule quast:
    input:#input is assembly files after filtering with coverage and length AND Reference genoem file
        filtered=renaming_res+"{sample}.fasta",
        ref=config["refstrain"]
    output:#collect_quast creates directory for each sample inlcuding tsv html ,...
        outdir1=directory(quast_res+"{sample}"),
    benchmark:
        quast_bench+"{sample}.txt"
    log:
        quast_log+"{sample}.txt"
    params:
        additional=config["quast_params"]
    conda:
      envs+ "quast.yaml"
    shell:
       "quast.py --conserved-genes-finding  -o {output.outdir1}  {input.filtered} {params.additional}  &>  {log} || true"

"""
Collects Quality of Assemblies
"""
rule collect_quast:
    input:#input is quast results for each sample
        localres=expand(quast_res+"{sample}", sample=SAMPLES),
        metadata=config['metadata'],
    output:#output is csv and xls
        allcsv=quast_res+"allquast.csv",
        allxls=quast_res+"allquast.xlsx",
    benchmark:
        quast_bench+"allquast.txt",
    log:
        quast_log+"allquast.txt",
    conda:
        envs+ "rxls.yaml"
    script:
        "../scripts/combinequast.R"#converts to xls
