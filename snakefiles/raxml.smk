"""
RAxML
Runs RAxML on mutliple sequence alignment from snippy
Returns tree with IDs in nodes as newick
"""
rule raxml:
    input:
        aln= snippy_core+"core.full.aln"
    benchmark:
        snippy_bench+"snippy_raxml.txt"
    log:
        snippy_log+"snippy_raxml.log"
    output:
        dir=directory(raxml_res_snippy)
    conda: envs +"raxml.yaml"
    threads:
        30
    params:
        config["raxml_params"]
    shell:
        "mkdir {output.dir} &&\
	raxmlHPC-PTHREADS -T {threads} -s {input.aln} -n snippy_raxml.nw -w {output.dir} {params} 2>{log} "

rule raxmlcopy:
    input:
        dir= raxml_res_snippy
    output:
        tmp+"snippy_raxml.nw"
    log:
        snippy_log+"snippy_raxmlcopy.log"
    shell:
        "cp {input.dir}/RAxML_bestTree.snippy_raxml.nw {output} 2>{log} "




"""
Draws tree from snippy results
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed samples
"""

rule drawSnippytree:
    input:
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        #Always us NEwick as variable name
        newick= tmp+"snippy_raxml.nw" #standard is RAxML if user wants fasttree it's writin in config
    output:
        #Always use pdf as variable name
#        pdf=trees+"snippy_fasttree.pdf",
        newick=trees+"snippy.nw"#tree in newick format with exchanded IDs into common names
    params:
        #alsways use params as variable name
        title="Phylogenetic tree from Snippy"
    conda:
        envs +"rcran.yaml"
    log:
        snippy_log+"drawSnippytree.log"
    benchmark:
        snippy_bench+"drawSnippytree.txt"
    script:
        "../scripts/plottree.R"

"""
RAxML
Runs RAxML on mutliple sequence alignment from panaroo
Returns tree with IDs in nodes as newick
"""
rule raxml_panaroo:
    input:
        aln= panaroo_res
    benchmark:
        panaroo_bench+"panaroo_raxml.txt"
    log:
        panaroo_log+"panaroo_raxml.log"
    output:
        dir=directory(raxml_res_panaroo)
    conda: envs +"raxml.yaml"
    threads:
        30
    params:
        config["raxml_params"]
    shell:
        "mkdir {output.dir} &&\
	raxmlHPC-PTHREADS -T {threads} -s {input.aln}/core_gene_alignment_filtered.aln -n panaroo_raxml.nw -w {output.dir} {params} 2>{log} "

rule raxmlcopy_panaroo:
    input:
        dir= raxml_res_panaroo
    output:
        trees+"panaroo_raxml.nw"
    log:
        panaroo_log+"panaroo_raxmlcopy.log"
    shell:
        "cp {input.dir}/RAxML_bestTree.panaroo_raxml.nw {output} 2>{log} "
