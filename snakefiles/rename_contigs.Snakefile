"""
depracted
Filtering contigs based on minimal coverage  5 and length 500.
"""
#rule filtering:
#    input:#input is result from spades
#        fasta=spades_res+"{fastq}",
#        scripts=scripts
#    output:
#        filtered=filtering_res+"{fastq}_filtered.fasta"
#    log:
#        filtering_log+"{fastq}.log"
#    conda:
#        "../envs/biopython.yaml"
#    benchmark:
#        filtering_bench+"{fastq}.txt"
#    shell:#script implemented by Eric Zuchantke
#        "python {input.scripts}/spades_cutoff.py {input.fasta}/contigs.fasta 5 500 {output.filtered} &>  {log}"


"""
Renames contigs to have short contig names (required by some tools)
"""
rule renameContigs:
    input:#input is result from spades
        fasta=rawassembly_res+"{sample}",
    output:
        renamed=renaming_res+"{sample}.fasta"
    log:
        filtering_log+"{sample}.log"
    conda:
        envs+"bioawk.yaml"
    benchmark:
        filtering_log+"{sample}.txt"
    shell:
        "{scripts}fa_rename.sh {input.fasta}/contigs.fa {output.renamed} &>  {log}"
