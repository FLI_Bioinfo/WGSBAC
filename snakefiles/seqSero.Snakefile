""""
Determining Salmonella serotypes using seqSero from reads.
"""
rule seqSero_reads:
    input:#input is raw reads
        fw=unzip_res+"{fastq}_R1.fastq",
        rv=unzip_res+"{fastq}_R2.fastq",
        #fasta=renaming_res+"{sample}.fasta"
    output:
        seqSero=directory(seqSero_reads+"{fastq}"), #results only from reads
        #results=seqSero_reads+"{fastq}"
    threads:
        4
    params:
        additional=config["seqsero_params"]
    log:
        seqSero_log+"{fastq}_reads.log"
    conda:
        envs+ "seqsero.yaml"
    benchmark:
        seqSero_bench+"{fastq}_reads.txt"
    shell:
        "(SeqSero2_package.py -p {threads} -t 2 -d {output.seqSero}  -i {input.fw} {input.rv} {params.additional} >{log} 2>&1) || touch {output.seqSero}"


"""
Collects seqSero results (coming from reads) into one table (CSV and XLS)
"""
rule combine_seqSero:
    input:#input is
        seqSero_reads=expand(seqSero_reads+"{fastq}", fastq=FASTQ), #results from reads
#        seqSero_contigs=expand(seqSero_contigs+"{sample}", sample=config["samples"]), #results from contigs
        metadata=config["metadata"]
    output:#outputs as csv and xls
        allcsv=seqSero_res+"allseqSero.csv",
        allxlsx=seqSero_res+"allseqSero.xlsx",
        multiqc=seqSero_res+"SeqSero_results_mqc.txt",
    log:
        seqSero_log+"seqSero.log"
    conda:
        envs + "rxls.yaml"
    benchmark:
        seqSero_bench+"seqSero.log"
    script:
        "../scripts/combineSeqSero.R" #create the table
