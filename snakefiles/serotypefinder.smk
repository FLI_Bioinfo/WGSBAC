
#downlads ecoh and ecovir if older than 30 days
rule ecohvirdownload:
    output:
        db=config["abricateecoli"],
    log:
        amr_log+"downloadabricateecoli.log"
    benchmark:
        amr_bench+"downloadabricateecoli.txt"
    conda:
        envs+ "abricate.yaml"
    shell:
        " abricate-get_db --db ecoh --force  2>{log}&&\
	abricate-get_db --db ecoli_vf --force  2>{log}&&\
	touch {output.db} && chmod 777 {output.db}  2>>{log}"



rule abricate_ecoli:
    input:
        fasta=renaming_res+"{sample}.fasta",
	    db=config["abricateecoli"],
    output:
        serofinder=ecolitype_res+"{sample}_ecoh.csv",
        ecoli_vf=ecolitype_res+"{sample}_ecoli_vf.csv",
    log:
        ecolitype_log+"{sample}.log"
    threads: 1
    benchmark:
        ecolitype_bench+"{sample}.txt"
    conda:
       envs+"abricate.yaml"
    params:
      additional=config["ecoliserotype_params"],
    shell:
      "(abricate --threads {threads} {input.fasta}  --db ecoh {params.additional} > {output.serofinder} 2>>{log}) || tourch {output.serofinder} &&\
      (abricate --threads {threads} {input.fasta}  --db ecoli_vf {params.additional} > {output.ecoli_vf} 2>>{log}) || tourch {output.ecoli_vf}"


"""
Combines/summarizes Abricate results in one single csv
"""
rule summary_abricate_ecoli:
    input:
        serofinder=expand(ecolitype_res+"{sample}_ecoh.csv", sample=SAMPLES),
        ecoli_vf=expand(ecolitype_res+"{sample}_ecoli_vf.csv", sample=SAMPLES),
    output:
        sero=(abricate+"ecolisero.csv"),
        ecoli_vf=(abricate+"ecoli_vf.csv"),
    log:
        ecolitype_log+"combine_abricate_ecolisero.log",
    benchmark:
        ecolitype_bench+"combine_abricate_ecolisero.txt"
    conda:
        envs + "abricate.yaml"
    shell:
       "abricate  --summary  {input.serofinder}   >{output.sero} 2>>{log} &&\
        abricate  --summary  {input.ecoli_vf}   >{output.ecoli_vf} 2>>{log}"


rule collect_abbricate_ecoliserotype:
    input:
        abricateres=abricate+"ecolisero.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=ecolitype_res+"all_ecoh.csv",#output csv format
        abricate_xls=ecolitype_res+"all_ecoh.xls",#output XLS format
        abricate_mqc=ecolitype_res+"Abricate_ecoh_mqc.txt",#output MQC format
    params:
        db="ecoh"
    log:
        ecolitype_log+"collect_abbricate_ecoliserotype.log"
    benchmark:
        ecolitype_bench+"collect_abbricate_ecoliserotype.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls



rule collect_abbricate_ecolivir:
    input:
        abricateres=abricate+"ecoli_vf.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=ecolitype_res+"all_ecoli_vf.csv",#output csv format
        abricate_xls=ecolitype_res+"all_ecoli_vf.xls",#output XLS format
        abricate_mqc=ecolitype_res+"Abricate_ecoli_vf_mqc.txt",#output MQC format
    params:
        db="ecoli_vf"
    log:
        ecolitype_log+"collect_abbricate_ecoliserotype.log"
    benchmark:
        ecolitype_bench+"collect_abbricate_ecoliserotype.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls
