"""
Assembly of the reads using shovill.
"""
rule shovill:
    input:#receive all forward and reverse fastq files
        fw=unzip_res+"{fastq}_R1.fastq",
        rv=unzip_res+"{fastq}_R2.fastq"
    output:
        directory(rawassembly_res+"{fastq}")#output is directoy for each sample
    threads:
        16
    params:
        additional=config["shovill_prarams"]
    benchmark:
        shovill_bench+"{fastq}.txt"
    log:
        shovill_log+"{fastq}.log"
    conda:
      envs+ "shovill.yaml"
    shell:#spaded does quality control and trimming itself
        "shovill --cpus {threads} \
            --R1 {input.fw} \
            --R2 {input.rv}\
            --force \
             {params.additional}\
            --outdir {output}\
            2> {log} ||\
	    touch {output}/contigs.fa" #creates empty file if shovil failed


