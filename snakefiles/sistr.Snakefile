""""
Salmonella In Silico Typing Resource (SISTR): for typing and subtyping Salmonella genome assemblies
"""
rule sistr:
    input:#input is final assemply results
        fasta=renaming_res+"{sample}.fasta"
    output:
        alleles=sistr_res+"{sample}/alleles-results.json",
        novelalleles=sistr_res+"{sample}/novel-alleles.fasta",
        cgmlst=sistr_res+"{sample}/cgmlst-profiles.csv",
        out=sistr_res+"{sample}/sistr-output.tab",
    threads:
        4
    conda:
        envs+"sistr_cmd.yaml"
    params:
        additional=config["sistr_params"]
    log:
        sistr_log+"{sample}.log"
    benchmark:
        sistr_bench+"{sample}.txt"
    shell:
        "(sistr --qc -t {threads} -vv --alleles-output {output.alleles} --novel-alleles {output.novelalleles} --cgmlst-profiles {output.cgmlst} -f tab -o {output.out} {params.additional} {input.fasta} 2> {log})|| (touch {output.out}; touch {output.cgmlst};touch {output.alleles}  ;  touch {output.novelalleles})"

"""
Collects SISTR results (given by sistr-output) into one table (CSV and XLS)
"""
rule combine_SISTR:
    input:#input is
        sistres=expand(sistr_res+"{sample}/sistr-output.tab", sample=SAMPLES),
        metadata=config["metadata"],
    output:#outputs as csv and xls
        allcsv=sistr_res+"allSISTRres.csv",
        allxlsx=sistr_res+"allSISTRres.xlsx",
        multiqc=sistr_res+"SISTR_results_mqc.txt",
    log:
        sistr_log+"allSISTR.log"
    benchmark:
        sistr_bench+"allSISTR.log"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineSISTR.R"#converts to write_xlsx
