"""2019.03.29 MYA"""


"""
snippy
Runs snippy for each fastq file
"""
rule snippy:
    input:#receive all forward and reverse fastq files
        fw= unzip_res+"{fastq}_R1.fastq",#unzipped fastq files
        rv= unzip_res+"{fastq}_R2.fastq",
        ref=config["refstrain"]#reference
    output:
        snippy= directory(snippy_folder+"{fastq}"),
    threads:
        8
    params:
        additional=config["snippy_params"]
    benchmark:
        snippy_bench+"{fastq}.txt"
    log:
        snippy_log+"{fastq}.log"
    conda:
      envs +"snippy.yaml"
    shell:
        "snippy --force  --cpus {threads} --outdir {output.snippy} --ref {input.ref} --R1 {input.fw} --R2 {input.rv} {params.additional} 2>{log}"

"""
snippy_core
Combines snippy into multiple  alignment
"""
rule snippy_core:
    input:
        snippylocal=expand( snippy_folder + "{fastq}", fastq=FASTQ),#snippy results from each single fastq files
        ref=config["refstrain"]
    benchmark:
        snippy_bench+"snippy_core.txt"
    log:
        snippy_log+"snippy_core.log"
    output:#all output from snippy-core
        aln= snippy_core+"core.aln",
        full= snippy_core+"core.full.aln",
        ref= snippy_core+"core.ref.fa",
        tab= snippy_core+"core.tab",
        txt= snippy_core+"core.txt",
        vcf= snippy_core+"core.vcf",
    conda: envs +"snippy.yaml"
    shell:#use prefix and store results in tmp directory, afterwards move them to final directory
        "snippy-core  --ref {input.ref} {input.snippylocal} --prefix tmp  2>{log}&&\
        mv tmp.aln {output.aln}&&\
        mv tmp.full.aln {output.full}&&\
        mv tmp.ref.fa {output.ref}&&\
        mv tmp.tab {output.tab}&&\
        mv tmp.txt {output.txt}&&\
        mv tmp.vcf {output.vcf}"


"""
snpdist
Convert a core alignment from snippy to SNP distance matrix
"""
rule snpdist:
    input:
        aln= snippy_core+"core.aln",
    benchmark:
        snippy_bench+"snpdist.txt"
    log:
        snippy_log+"snpdist.log"
    output:#all output from snippy-core
        snpdist= snippy_core+"snpdist.csv",

    conda: envs +"snpdist.yaml"
    shell:
        "snp-dists {input.aln} > {output.snpdist} 2>{log}"


"""
Merges snpdist data with metaData. Creates XLS
"""
rule combine_snpdist:
    input:
        snpdist=snippy_core+"snpdist.csv",
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
    benchmark:
        snippy_bench+"combine_snpdist.txt"
    log:
        snippy_log+"combine_snpdist.log"
    output:#all output from snippy-core
        xls= snippy_core+"snpdist.xls",
        multiqc=snippy_core+"Snipdist_mqc.txt",
    conda: envs +"rxls.yaml"
    script:
        "../scripts/combineSNPdist.R"


"""
fasttree
Runs fasttree on mutliple sequence alignment from snippy
Returns tree with IDs in nodes as newick
"""
rule fasttree:
    input:
        aln= snippy_core+"core.aln"
    benchmark:
        snippy_bench+"snippy_fasttree.txt"
    log:
        snippy_log+"snippy_fasttree.log"
    output:
        tree=(tmp+"snippy_fasttree.nw")
    conda: envs +"fasttree.yaml"
    shell:
        "FastTree -gtr -nt {input.aln} > {output.tree} 2>{log}"




"""
hier clustering of snpdist
hierbaps of snp.core
"""
rule clust_snpdist:
    input:
        snpdist=snippy_core+"snpdist.csv",
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        aln= snippy_core+"core.aln",
    benchmark:
        snippy_bench+"clust_snpdist.txt"
    params:
        #alsways use params as variable name
        maxsnp=config['hclust_maxsnp'],#200,100,50,20,10,5,1
        minmember=config['hclust_minmember'],#2
        maxdepth = config['hierBAPS_maxdepth'],#4,
        npops =config['hierBAPS_npops'],# 50
    log:
        snippy_log+"clust_snpdist.log"
    output:#all output from snippy-core
        hclust= clustering+"hclust_snps.xls",
        htree=trees+"hclust_snpdist.nw",
        hierBAPS=clustering+"hierBAPS_snps.xls",
        clustall=temp(tmp+"clustall_snpcore.csv"),
        cluststats=clustering+"cluststats_snps.xls"
    conda: envs +"rclust.yaml"
    script:
        "../scripts/clustSNPs.R"
