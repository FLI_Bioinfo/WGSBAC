"""
Run sourmash  on each assembly of fastq files.
performs k mer based classification into genus,species and strains
computes sig file for each assemly
"""
rule sourmash_compute:
    input:#input is final assemblies
        fasta=renaming_res+"{sample}.fasta",
    output:#result from cansnper
        sig=sourmash_res+"{sample}.sig",
    conda:
        envs + "sourmash.yaml"
    log:
        sourmash_log+"{sample}_sig.log"
    benchmark:
        sourmash_bench+"{sample}_sig.txt"
    params:
        additional=config["sourmash_compute_params"]
    shell:
        "sourmash sketch dna  {params.additional} {input.fasta}  -o {output.sig} 2>{log} || touch {output.sig}"
"""
Run sourmash  on each assembly of fastq files.
performs k mer based classification into genus,species and strains
classifies based on sig file
"""
rule sourmash_classifiy:
    input:#input is final assemblies after spades and filtering with coverage and length AND CanSNPEr DB
        sig=sourmash_res+"{sample}.sig",
        db=config["sourmashDB"]
    output:#result from cansnper
        classification=sourmash_res+"{sample}_classify.txt",
        summary=sourmash_res+"{sample}_summary.txt",
    conda:
        envs + "sourmash.yaml"
    log:
        sourmash_log+"{sample}_classy.log"
    benchmark:
        sourmash_bench+"{sample}_classy.txt"
    params:
        additional=config["sourmash_params"]
    shell:##allow differences is needed for francisella only, otherwise results are wrong
        "(sourmash lca classify {params.additional} --query {input.sig} --db {input.db} >{output.classification} 2>{log} || touch {output.classification}) &&\
        (sourmash lca summarize  {params.additional} --query {input.sig} --db {input.db} >{output.summary} 2>>{log} || touch {output.summary})"

"""
Collects sourmash results
output is xls, csv and inout for mutliqc
"""
rule collect_sourmash:
    input:#input is
        sourmash=expand(sourmash_res+"{sample}_classify.txt", sample=SAMPLES),
        metadata=config['metadata'],
    output:#outpus as csv and xls
        allcsv=sourmash_res+"allSourmash.csv",
        allxls=sourmash_res+"allSourmash.xlsx",
        multiqc=sourmash_res+"Sourmash_mqc.txt",
    log:
        sourmash_log+"collectsourmash.log"
    benchmark:
        sourmash_bench+"collectsourmash.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/combineSourmash.R"#converts to xls
