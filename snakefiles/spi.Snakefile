
#abricate for Salmonella pathogenic islands
rule abricate_spi:
    input:
        fasta=renaming_res+"{sample}.fasta",
    output:
        virulence_res+"{sample}_spi.csv",
    log:
        virulence_log+"{sample}_spi.log",
    threads: 1
    benchmark:
        virulence_bench+"{sample}.txt"
    params:
        additional=config["spiabricate_params"],
        datadir=config["spifolder"],#path to SPI abricate dab
    conda:
      envs + "abricate.yaml"
    shell:
      "abricate --threads {threads} {input.fasta}  --db SPI --datadir {params.datadir}  {params.additional} >{output} 2>>{log}"

"""
Combines/summarizes Abricate results in one single CSV
"""
rule summary_spi:
    input:
        spi=expand(virulence_res+"{sample}_spi.csv", sample=SAMPLES),
    output:
        temp(abricate+"spi.csv"),
    log:
        virulence_log+"combine_abricate_spi.log",
    benchmark:
        virulence_bench+"combine_abricate_spi.txt"
    conda:
      envs + "abricate.yaml"
    shell:
      "abricate  --summary  {input.spi}   >{output} 2>>{log}"

rule collect_abbricate_spi:
    input:#input is
        abricateres=abricate+"spi.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=virulence_res+"all_spi.csv",#output csv format
        abricate_xls=virulence_res+"all_spi.xls",#output XLS format
        abricate_mqc=virulence_res+"abricate_spi_mqc.txt",#output MQC format
    params:
        db="spi"
    log:
        virulence_log+"collect_abbricate.log"
    benchmark:
        virulence_bench+"collect_abbricate.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls
