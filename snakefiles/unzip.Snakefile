"""
Unzip
Needed fpr coverage calculation
"""
rule unzip:
    input:#receive all forward and reverse fastq files
        fw= input+"{fastq}_R1.fastq.gz",
        rv= input+"{fastq}_R2.fastq.gz",
    output:#files are marked as temp and deleted after running pipeline
      fw_u=unzip_res+"{fastq}_R1.fastq",
      rv_u=unzip_res+"{fastq}_R2.fastq"
    benchmark:
      unzip_bench+"{fastq}.txt"
    log:
      unzip_log+"{fastq}.log"
    shell:#standard gunzip
      "gunzip -c {input.fw} > {output.fw_u} 2>{log} &\
       gunzip -c {input.rv} > {output.rv_u} 2>>{log}"
