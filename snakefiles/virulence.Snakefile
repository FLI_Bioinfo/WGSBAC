

#downlads vfdb if older than 30 days
rule vfdbdownload:
    output:#directory holding all mlst results for each sample
        db=config["abricatevir"],
    log:
        amr_log+"downloadabricatevfdb.log"
    benchmark:
        amr_bench+"downloadabricatevfdb.txt"
    conda:
        envs+ "abricate.yaml"
    shell:
        " abricate-get_db --db vfdb --force  2>{log}&&\
	touch {output.db} && chmod 777 {output.db}  2>>{log}"


"""
Runs Abricate using assembly as input against following databases:
-vfdb core
"""
rule abricate_vfdb:
    input:
        fasta=renaming_res+"{sample}.fasta",
	db=config["abricatevir"],
    output:
        vfdb=virulence_res+"{sample}_vfdb.csv",
    log:
        vfdb=virulence_log+"{sample}_vfdb.log",
    threads: 1
    benchmark:
        virulence_bench+"virulencefinder.{sample}.txt"
    params:
        additional=config["vfdb_abricate_params"]
    conda:
        envs + "abricate.yaml"
    shell:
      "abricate --threads {threads} {input.fasta}  --db vfdb  {params.additional} >{output.vfdb} 2>>{log.vfdb}"


"""
Combines/summarizes Abricate results in one single CSV
"""
rule summary_vfdb:
    input:
        vfdb=expand(virulence_res+"{sample}_vfdb.csv", sample=SAMPLES),
    output:
        vfdb_csv=temp(abricate+"vfdb.csv"),
    log:
        vfdb=virulence_log+"combine_abricate_vfdb.log",
    benchmark:
        virulence_bench+"combine_abricate.txt"
    conda:
        envs + "abricate.yaml"
    shell:
        "abricate  --summary  {input.vfdb}   >{output.vfdb_csv} 2>>{log.vfdb}"


rule collect_abbricate_vfdb:
    input:
        abricateres=abricate+"vfdb.csv",#temp file after abricate summary
        metadata=config['metadata'],
    output:#outpus as csv and xls
        abricate_csv=virulence_res+"all_vfdb.csv",#output csv format
        abricate_xls=virulence_res+"all_vfdb.xls",#output XLS format
        abricate_mqc=virulence_res+"Abricate_vfdb_mqc.txt",#output MQC format
    params:
        db="vfdb"
    log:
        virulence_log+"collect_abbricate_vfdb.log"
    benchmark:
        virulence_bench+"collect_abbricate_vfdb.txt"
    conda:
        envs + "rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls
