#!/usr/bin/perl
#WGSBAC is a pipeline for genotyping and characterisation of bacterial isolates utilizing whole-genome-sequencing (WGS) data
# Author: Jörg Linde, Firedrich Loeffler Institute Jena
#License: https://gitlab.com/FLI_Bioinfo/WGSBAC
#Help an description https://gitlab.com/FLI_Bioinfo/WGSBAC
#

#load perl libs
use strict;
use warnings;

use Getopt::Long qw(GetOptions);#reading command line arguments
use Cwd qw(getcwd);
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use lib dirname(abs_path $0)  . '/scripts/perl5lib';#reads libraries in scripts/perl5lib
use WGSBAC::Checkmeta qw(checktable);#lib to check meta data template
use WGSBAC::Initfolders qw(initfolders);#lib to initialize folders
use WGSBAC::Checkparams qw(checkparams);#lib to check command line parameters
use WGSBAC::Download qw(download);#lib to download data
use WGSBAC::Config qw(config);#lib to wirte snakemake config file
use WGSBAC::Snakefile qw(snakefile);#lib to wirte snakemake file
use WGSBAC::Linking qw(link_samples);#lib to link input data from table inside file strucutre of the pipeline
use File::Basename;

#welcome user and give information
my $VERSION = '2.2.3';
my $AUTHOR = 'Jörg Linde';
my $User=$ENV{USER} || 'stranger';
my $Script=basename($0);

print "Hello $User\n" ;
print "This is WGSBAC $VERSION \n";
print "a pipeline for genotyping and characterisation of bacterial isolates utilizing whole-genome-sequencing (WGS) data\n";
print "Send complaints to $AUTHOR\n";
sleep(2);
print "Here you can find help and information https://gitlab.com/FLI_Bioinfo/WGSBAC \n";
print "Or use wgsbac.pl --help\n";
print "Checking input and configuring pipeline\n";
sleep(2);


#creates help message
my $usage="
Usage: $Script  --(t)able <metatdata.csv>   --(k)raken <path to Kraken2 DB>  <Optional Parameters>\n
###############################\n
### Obligatory Parameters:\n###
###############################\n
--(t)able String.Complete path to metadata mable (tab delimited) holding columns:ID,Common.name,Type,File_left_reads,File_right_reads,File_assembly,Lat.Long.of.Isolation\n
        Type describes type of isolate and can be any of:a)  reference [only once], b) collected c) outbreak\n
        File_left/right_reads hold path to gziped raw sequencing readsinf FASTQ format\n
        File_assembly for allready assembled reference strains (not needed if FASTQ files exist)\n
        Do not use . or _ or whitespaces in ID or common.name\n
        See https://gitlab.com/FLI_Bioinfo/WGSBAC  for details and examples\n
--(k)raken String. Complete path to kraken2DB folder. You may download Minikraken2 here ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/ \n
Running only with obligatory parameters, WGSBAC will run FASTQ, calculate coverage, assembles genomes,run Quast on genomes and identify the genus and species using Kraken and MASH\n
########################\n
###Optional Parameter###\n
########################\n
--(h)elp Helpful message\n
\n
########################\n
###Optional Configuration of Pipeline###\n
########################\n
--(r)esults  String. Absolute path to result directory. Standard <current-dir>/result/\n
--(l)og  String. Complete path to log directory. Standard results/log\n
--run Flag. If used pipeline is started automatically.\n
--cpus String. Number of cpus to run the pipeline. Standard is 40.  Only needed when using option run\n
--profile String. Path to folder holding Snakemake profile for SLURM support.  Find an example here slurmprofile/config.yaml and copy to /home/<user>/.config/snakemake/


\n
########################\n
###Optional Configuration of Assembly###\n
########################\n
--(ad)atrim String. If used adapters are trimmed by trimmomatic\n
--min(cov) Integer. Filtering of assembly with minimum contig k-mer coverage minimum contig length.\n
--(min)len Integer. Filtering of assembly minimum contig length.\n
--(pr)okka Flag. If used assembled genomes are annotated using Prokka.\n
--(ba)kta String. Complete Path to Bakta DB. If used assembled genomes are annotated using Bakta.\n

########################\n
###Optional Serotyping###\n
########################\n
--(si)str Flag. If used Sistr is run for Salmonella serotyping.\n
--(se)qsero Flag. If used SeqSero2 is run for Salmonella serotyping.\n
--(ecoli)type Flag. If used identifies Clermont phylogrups and runs abricate against databases ecoh  and ecoli_vf.\n

########################\n
###Optional Genotyping###\n
########################\n
--(conf)indr String. Absolute path to ConFindR rMLST database. If used ConFindR is run. \n
--(ml)st  String. Mlst scheme name. If used mlst is run.\n
--(c)ansnper String. If used Cansnper is run to predict canonical SNP types. Select a species from data/cansnper_species.txt.\n
--(c2)ansnper2 String. Name of CanSNPer2 database. Currently supports: bacillus_anthracis.db, francisella_tularensis.db, yersinia_pestis.db. If used CanSNPer2 is run to predict canonical SNP types.\n
--(pa)rsnp Flag.If used parsnp is run.\n
--(sn)ippy Flag. If used snippy is run.\n
--(r)axml Flag. If used RAxML is run based on snippy core and/or panaroo.\n
--(fa)sttree Flag. If used fasttree is run to create tree from snippy core alignment. Standard: RAxML
--(pa)naroo Flag. If used panaroo is run. This options needs bakta and the bakta DB\n
--(ml)va String. One of 'Anthracis', 'Brucella', 'Burkholderia', 'Coxiella'. If used runs MLVA_finder https://github.com/i2bc/MLVA_finder\n
########################\n
###Phenotyping###\n
########################\n
--(amr) Flag if used  runs AMRfinderplus and abricate against CARD, NCBI and resfinder.
--amr(org)anism String Option for AMRfinderplus. You may use organism-sepcific results for: Campylobacter,Enterococcus_faecalis, Enterococcus_faecium, Escherichia,Klebsiella,Salmonella,Staphylococcus,Vibrio_cholerae. Find etailts https://github.com/ncbi/amr/wiki/Running-AMRFinderPlus#--organism-option \n
--(vir)ulence FLag if used runs abricate against VFDB core.
--(plas)mids FLag if used runs a) abricate against plasmidfinder DB b) platon
--pathislands(spi) Flag  If used will run  abricate using database for Salmonella pathogenicity islands database https://gitlab.com/FLI_Bioinfo_pub/spis_ibiz_database \n
--clostox(ct)  Flag  If used will run abricate using database for Clostridia perfringens toxing typing https://gitlab.com/FLI_Bioinfo_pub\n
--arcodb(adb) Flag  If used will run abricate using Aliarcobacter butzleri - AMR and virulence databases https://gitlab.com/FLI_Bioinfo_pub/publicationdata_analysis-of-arcobacter-butzleri \n

########################\n
###Example usage###\n
########################\n
Brucella\n
wgsbac.pl --table meta_brucella.csv  --results /home/test/  --kraken /home/Kraken2DB/   -mlva Brucella   --snippy --raxml  \n
Salmonella\n
wgsbac.pl --table meta_salmonella.csv  --results /home/test/  --kraken /home/Kraken2DB/ --sistr --seqsero  --mlst  senterica_achtman_2 --snippy --raxml --amr  --amrorganism Salmonella --virulence --plasmids --pathislands  \n
Bacillus\n
wgsbac.pl --table meta_bacillus.csv  --results /home/test/  --kraken /home/Kraken2DB/ --mlva  Anthracis  --cansnper Bacillus_anthracis  --cansnper2 bacillus_anthracis.db --snippy --raxml --virulence \n
Ecoli\n
wgsbac.pl --table meta_ecoli.csv  --results /home/test/  --kraken /home/Kraken2DB/  --mlst ecoli   --ecolitype --amr --amrorganism Escherichia   --snippy  --raxml --virulence \n
Francisella\n
wgsbac.pl --table meta_francisella.csv  --results /home/test/  --kraken /home/Kraken2DB/    --snippy --raxml --cansnper Francisella --cansnper2  francisella_tularensis.db \n



";
#initializes standard variables
my ($help,$table,$kraken,$logfolder)="";
#initializes optional variables
my $binfolder=dirname(__FILE__)."/";
my $data=$binfolder."data";
#initializes optional variables
my ($refstrain,$sistr,$trim,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$mincov, $minlength,$run,$seqsero,$mlva,$fasttree,$amr,$organism,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$profile,$raxml);
$mincov=$minlength=$amr=$spi=$clostox=$arcodb=0;
$run=$trim=$prokka=$bakta=$snippy=$panaroo=$sistr=$mlst=$parsnp=$cansnper=$cansnper2=$sistr=$parsnp=$seqsero=$mlva=$fasttree=$organism=$refstrain=$confindr=$profile=$raxml="";
my $cpus=40;
my $mlst_schemes=$data."/scheme_species_map.tab";#lists available schemes
my $cansnp_species=$data."/cansnper_species.txt";#lists available species
my $resultsfolder="results/";#results folder
my $sourmash=$data."/gtdb-release89-k31.lca.json.gz" ;#index for sourmash
my $spifolder=$data."/SPI/" ;#abricate DB  for Salmonella pathogenicity islands
my $arcofolder=$data."/ArcoDB/" ;#abricate DB  for arcobacter
my $ctoxfolder=$data."/ClostridiaToxinDB/" ;#abricate DB  for arcobacter
my $platonfolder=$data."/Platon/" ;#folder holding platon data
my $abricatedbfolder=$data."/Markerupdates" ;#folder holding updates information of used abricate DBs data
my $cansnp2folder=$data."/CanSNPer2" ;#DB for CanSNPER2

#reads option from command line
    GetOptions(
        'table|t=s' => \$table,
        'kraken|k=s' => \$kraken,
        'help|h' => \$help,
        'results|r=s' => \$resultsfolder,
        'log|l=s' => \$logfolder,
        'mlst|ml=s' => \$mlst,
        'cansnper|c=s' => \$cansnper,
        'cansnper2|c2=s' => \$cansnper2,
        'mincov|cov=s' => \$mincov,
        'mlva|ml=s' => \$mlva,
        'minlen|min=s' => \$minlength,
        'cpus|cp=s' => \$cpus,
        'parsnp|pa' => \$parsnp,
        'prokka|pr' => \$prokka,
        'bakta|ba=s' => \$bakta,
        'snippy|sn' => \$snippy,
        'sistr|si' => \$sistr,
        'seqsero|se' => \$seqsero,
        'run' => \$run,
        'adatrim|ad' => \$trim,
        'panaroo|pa' => \$panaroo,
        'fasttree|fa' => \$fasttree,
        'amr|amr' => \$amr,
        'virulence|vir' => \$virulence,
        'plasmids|plas' => \$plasmids,
        'amrorganism|org=s' => \$organism,
        'pathislands|spi' => \$spi,
        'clostox|ct' => \$clostox,
        'arcodb|adb' => \$arcodb,
        'confindr|conf=s' => \$confindr,
        'ecolitype|ecoli' => \$ecoli,
	      'profile=s' => \$profile,
        'raxml|r' => \$raxml,


 ) or die ("$usage");


if($help ne "") {#print help message in case of --help or wron options
  die ("$usage");
}




if ((substr($resultsfolder,-1) ne "/")) {##adds / if missing
  $resultsfolder=$resultsfolder."/";
}



##checking input parameters
checkparams($table,$resultsfolder,$binfolder,$kraken,$mlst,$cansnper,$cansnper2,$mlst_schemes,$cansnp_species,$mlva,$amr,$organism,$bakta,$panaroo);

#download all needed data
download($sourmash,$spi,$spifolder,$arcodb,$arcofolder,$clostox,$ctoxfolder,$plasmids,$platonfolder,$mlst_schemes,$mlst,$amr,$virulence,$data,$ecoli,$abricatedbfolder);


checktable($table,$snippy,$parsnp);#checking  metatdata
#creates resultsfolders and variables for subfolders
my ($unzipfolder,$assemblyfolder,$inputfolder,$tmpfolder, $benchmarkfolder,$envfolder,$scriptsfolder,$snakefiles,$config,$conda)="";
#print "$config ini\n";



#initialize folder strucutre of pipeline
 ($unzipfolder,$assemblyfolder,$logfolder,$inputfolder,$tmpfolder, $benchmarkfolder,$envfolder,$scriptsfolder,$snakefiles,$config,$conda)=initfolders($resultsfolder,$binfolder);


##links files from metadata to inputfolder using IDs as names
 $refstrain=link_samples($table,$unzipfolder,$assemblyfolder,$inputfolder);


#writes config file
config($config,$refstrain,$table,$resultsfolder,$binfolder,$unzipfolder,$assemblyfolder,$logfolder,$inputfolder,$tmpfolder, $benchmarkfolder,
$envfolder,$scriptsfolder,$kraken,$snakefiles,$mlst_schemes,$cansnper,$cansnper2,$cansnp2folder,$sistr,$snippy,$parsnp,$prokka,$mincov, $minlength,$trim,$panaroo,$seqsero,$data,$sourmash,$mlva,$fasttree,$amr,$virulence,$plasmids,$platonfolder,$organism,$spi,$spifolder,$arcodb,$arcofolder,$clostox,$ctoxfolder,
$confindr,$ecoli,$abricatedbfolder,$mlst,$bakta,$raxml)  ;

#copies master.snakefile template to user-specific snakefile
#adds to the rule_all the results the user wants
my $snakefile=snakefile($sistr,$mlst,$cansnper,$cansnper2,$snippy,$parsnp,$prokka,$panaroo,$seqsero,$binfolder,$mlva,$amr,$virulence,$plasmids,$spi,$clostox,$arcodb,$confindr,$ecoli,$bakta,$raxml);

if(length($profile)>0){##profile file for snakemake SLURM support
        $profile="--profile ".$profile
}




#starts snakemake
print "Created config file: $config\n";
print "Configuration sucessfully finished\n";
sleep(3);
mkdir($logfolder);
if(!($run)){
  print "you may start the pipeline using:\n";
  print "snakemake  --snakefile $snakefile   --cores $cpus --use-conda  --conda-prefix $conda --configfile $config $profile --keep-going 2>&1 | tee $logfolder/snakemake.log\n ";
  print "To create SnakemakeReport.html holding versions of used tools run:\n";
  print "snakemake  --snakefile $snakefile    --use-conda  --conda-prefix $conda --configfile $config --report $resultsfolder/SnakemakeReport.html \n";
}
else{
    print "Starting pipeline now\n";
    print "snakemake  --snakefile $snakefile   --cores $cpus  --use-conda  --conda-prefix $conda --configfile $config  $profile  --keep-going 2>&1 | tee $logfolder/snakemake.log \n ";
    sleep(2);
    system("snakemake  --snakefile $snakefile   --cores $cpus  --use-conda  --conda-prefix $conda --configfile $config  $profile $profile --keep-going 2>&1 | tee $logfolder/snakemake.log\n");
    print "Please carefully check log file $logfolder/snakemake.log\n ";
    print "Creating SnakemakeReport.html holding versions of used tools\n";
    sleep(2);
    system ("snakemake  --snakefile $snakefile --use-conda  --conda-prefix $conda --configfile $config --report $resultsfolder/SnakemakeReport.html >/dev/null 2>&1");
}
